// TODO:
// - au reset, les listeners (sur window teleport, et syncing view) s'overwrite OU PAS ???? (pour pas les accumuler)

// ---  Creating the looping effect of the ring space ---

const wrapper = document.getElementById("scrolling-ring");
const space = document.getElementById("ring-space");
const floor = document.getElementById("floor");

let screen_type;

let start_view;
let end_view;
let sections_in_view;

const cut = true;
//the fraction of the ring_section that excess at the right of the screen
let cut_f;
let cut_w;


// UTILITY
// ===========================================================================

function detectMob() {
  //detect if mobile device
  return (window.innerWidth <= 600);
}

function detectScreen() {
  //detect the type of screen
  screen_width = window.innerWidth;
  if(screen_width<=600){
    type = 1;
  }
  else if(screen_width<=992){
    type = 2;
  }
  else{
    type = 3;
  }
  return type;
}

function is_scrollable(el){
  return el.scrollHeight > el.offsetHeight ? true : false;
}


// DARGGING
// ===========================================================================

function initDragging(){
  floor.addEventListener("mousedown", floor_mousedown);
  floor.addEventListener("mousemove", floor_mousemove);
  floor.addEventListener("mouseup", floor_mouseup);
  floor.addEventListener("mouseout", floor_mouseup);
}

var isPointerDown = false;
let pointerOrigin;

function floor_mousedown(event){
  pointerOrigin = event.clientX;
  isPointerDown = true;
}
function floor_mousemove(event){
  if (!isPointerDown) {
    return;
  }
  event.preventDefault();
  let pointerPosition = event.clientX;
  let vector = pointerOrigin - pointerPosition;
  wrapper.scrollLeft += vector;
  pointerOrigin = event.clientX;
}
function floor_mouseup() {
  isPointerDown = false;
}

// LOOP EFFECT
// ===========================================================================

function loop_reset(){

  start_view = [];
  end_view = [];

  //on enlève les classes aux elems de l'ancienne start views
  var old_start_view = document.getElementsByClassName("start-view");
  if(old_start_view.length!=0){
    while (old_start_view[0]) {
      old_start_view[0].classList.remove("start-view")
    }
  }
  //on enlève les elements de end_view
  var old_end_view = document.getElementsByClassName("end-view");
  if(old_end_view.length!=0){
    while (old_end_view[0]) {
      old_end_view[0].parentNode.removeChild(old_end_view[0]);
    }
  }
  //NOTE: getElementsByClassName create a live tracking collection of html elements
  //that's why we have to use this while iterating technique
  //because by removing the classname, we also remove the element from the list
}

function loop_setup(){
  //setup the start and end view to match
  //on va copier un 1 ou plusieurs ring section du début a la fin (en fonction de la taille de l'écran)
  //pour que la start view et end view du looping effect match

  let old_screen_type = screen_type;
  screen_type = detectScreen();

  if(old_screen_type != screen_type){
    console.log("yes");

    //we changed display type
    loop_reset();

    //--- START VIEW CREATING ---
    //on crée la list d'elements de départ en fonction de mobile ou pas
    var screen_width = window.innerWidth;
    let ring_sections = space.getElementsByClassName("ring-section");
    for (var i = 0; i < ring_sections.length; i++) {
      var bounding_rect = ring_sections[i].getBoundingClientRect();
      var left = bounding_rect.left + wrapper.scrollLeft;
      if(left < screen_width){
        start_view.push(ring_sections[i]);
        console.log(ring_sections[i].id);
      }
    }
    for(var el of start_view){
      el.classList.add("start-view");
    }

    //--- CLONING ---
    //on clone les elems de la start view a la fin ou il y a la coupure
    for(var el of start_view){
      var clone = el.cloneNode(true);
      clone.style.order = 1000 + getComputedStyle(el).order;
      end_view.push(clone);
    }
    for(var el of end_view){
      el.classList.remove("start-view");
      el.classList.add("end-view");
      space.appendChild(el);
    }

    //--- SYNCING ---
    sync_scroll_setup();

    //--- LOOPING ---
    loop_effect();
  }
  else{
    // nothing
  }
}

function loop_effect(){
  //on met un listener d'event sur la window pour que quand elle arrive au bouts
  //elle reparte du début et inversément (+1 ou -1 pour pas faire de loop infini)
  wrapper.addEventListener('scroll', function(e) {
    // console.log('scroll event');
    if (wrapper.scrollLeft < 1) {                                                //left
      wrapper.scrollLeft  = wrapper.scrollWidth - wrapper.clientWidth - 2;
    }
    if (wrapper.scrollWidth - 1 < wrapper.scrollLeft + wrapper.clientWidth) { //right
      wrapper.scrollLeft = 2;
    }

    //remember the scroll position between pages
    sessionStorage.setItem("scrollPos", wrapper.scrollLeft);
  });
}


// SYNCING VIEWS
// ===========================================================================

//TODO: match animation timing in setup()

var sync_scroll_timer = null;

function sync_scroll_setup(type){
  // pour que ça loop sans cut, il faut sync les scroll value dans la start_view et end_view
  // Note: on peut pas ajouter d'eventlistener sur chaque box car ça ralenti & bug sur firefox
  // du coup on change que au téléport!

  var start_vboxes = document.querySelectorAll(".start-view");
  var end_vboxes = document.querySelectorAll(".end-view");

  for (var i = 0; i < start_vboxes.length; i++) {

    let start_vbox = start_vboxes[i];
    let end_vbox = end_vboxes[i];

    //when initiating
    end_vbox.scrollTop = start_vbox.scrollTop;

    start_vbox.addEventListener('scroll', function(){sync_scroll(start_vbox, end_vbox)}, false);
    end_vbox.addEventListener('scroll', function(){sync_scroll(end_vbox, start_vbox)}, false);
  }
}

function sync_scroll(master, slave){
    // we want to update the scroll position when all the default scroll events are finished
    // to not interupt them (create weird call loops and glitch in firefox)
    // the timer is reseting every time a scroll event is called
    if(sync_scroll_timer !== null) {
      clearTimeout(sync_scroll_timer);
    }
    sync_scroll_timer = setTimeout(function() {
        // this trigger if not other scroll event where called in 150ms
        slave.scrollTop = master.scrollTop;
    }, 150);
}


// MAIN
// ===========================================================================

function setup(){

  loop_setup();

  // init dragging on the floor if not on modile
  if(!detectMob()){
    initDragging();
  }

  //get back to scroll position
  // if (sessionStorage.scrollPos != undefined){
  //   wrapper.scrollLeft = sessionStorage.scrollPos;
  // }
}

//on met un listener d'event sur quand on resize la fenêtre pour relancer le setup
window.addEventListener('resize', setup);

setup();

// put the wrapper in focus
// so we can directly scroll with arrows, page up/down and space
wrapper.focus();
