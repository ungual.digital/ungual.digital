title: me
sort: float

![me, doriane](/images/me/IMG_20240720_132945_613.jpg)

I'm Doriane. I grew up in Belgium and live in Bruxelles.
I have an infinite love for the idea of designing with words and language.
I like to think about systems and automatised processes as sensible mediums.
I try to question tools by looking on what they make us do, as much as what they do.

In 2021 I joined [Open Source Publishing](https://osp.kitchen), where i make websites & conversations.
<!-- and experiment with the blurry edges of `HTML` & `CSS` as a medium and textual material. -->

Since 2023, I coordinate an artistic research on `CSS` and the crafts of designing with language, called [Declarations](https://declarations.ungual.digital).

I deeply and sincerly love cooking, and walking (both alone or with other people).
I've a feral desire to be more than one.

<!-- i'm interested in
forms of poetry emerging from our dayli interactions with digital material,
ways to reconnect/reappropriate with the modern informations ecosystems,
and the idea of an interface. -->
