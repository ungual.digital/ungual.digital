title: wiki scratching
date: 2024 03
thumbnail: /images/teaching/wiki-scratching.jpg
tags: css, declarative, wikipedia, etherpad
summary: A workshop at [le 75](https://leseptantecinq.be/fr/) about giving new themes to specific articles of wikipedia in collaborative way using etherpad.