title: Declarative Companion
date: 2023 03
tags: html, css, declaratives
summary: A workshop at [le 75](https://leseptantecinq.be/fr/) about describing everyday objects and crafting webpages out of those descriptions.
link: https://etudiants.le75.be/2022-2023/B3/declarative-compagnon/
thumbnail: /images/teaching/compagnon.png