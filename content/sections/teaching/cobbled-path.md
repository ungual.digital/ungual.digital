title: Cobbled paths
date: 2024 03
tags: ascii, typography, plotter
thumbnail: /images/teaching/cobbled.jpg
summary: A workshop with gijs de heij at besancon on an experimental tool that convert ascii FIGlet font to continuous svg in order to pen-plot them.