title: Etherdot
date: 2023 06
tags: graphviz, declaratives, etherpad
summary: A workshop with Gijs de Heij at [Karlsruhe](https://hfg-karlsruhe.de/en/hochschule/) about using [Graphviz](https://graphviz.org/) DOT language to create graphs on technological standards through etherpad.
link: http://osp.kitchen/workshop/karlsruhe/
thumbnail: /images/teaching/etherdot.png