title: etherstyling Wikipedia
date: 2023 04
tags: css, web2print, wikipedia, etherpad
summary: A workshop with Amélie Dumont at [Mainz](https://www.hs-mainz.de/studium/studiengaenge/gestaltung/bachelor-kommunikationsdesign/uebersicht/) about giving new themes to specific articles of wikipedia in collaborative way using etherpad.
link: http://osp.kitchen/workshop/mainz/
thumbnail: /images/teaching/wikipedia.png