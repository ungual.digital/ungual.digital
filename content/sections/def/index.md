title: ungual & digital
date: 2023 04

<dl>
    <dt>
        digital
    </dt>
    <dd>
        relating to a finger or fingers.
    </dd>
    <dt>
        ungual
    </dt>
    <dd>
        relating to or affecting a nail, hoof, or claw.
    </dd>
</dl>

![a screenshot of the codex seraphinianus with a nail writting](/images/ungual2.png)