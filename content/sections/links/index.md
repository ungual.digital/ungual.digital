title: Links semantic
date: 2023 06

from sky to earth

* <span class="external">external links</span> (different domain)
* <span class="sub">subdomain links</span> (subdomain)
* <span class="internal">internal links</span> (same domain)
* <span class="internal local">anchors links</span> (same page)
