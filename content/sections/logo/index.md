title: logo
sort: first

<div class="logo">
<a class="logo-html" href="/"></a>
<style class="logo-css">.logo-html{ 
    display: block; 
    box-sizing: border-box; 
    font-size: 24px; 
    border: 2em white ridge; 
    height: 6.5em; 
    width: 4em; 
    border-radius: 50% 50% 50% 50% / 60% 60% 40% 40%; 
    border-left-width: 0; 
    filter:  url(#svgThreshold); 
    margin: auto; 
    transition: 1s border;   
    cursor: crosshair; 
    mix-blend-mode: darken;
}
.logo-css{ 
    display: block; 
    box-sizing: border-box;
    height: var(--side-menu);
    padding: 2rem 0;
    column-width: 12rem;
    column-fill: auto;
    white-space: pre-wrap; 
    font-family: monospace;
    line-height: var(--lh-mono);
    font-size: 0.66rem;
    color: var(--clr-fade); 
    opacity: 0; 
    transition: 1s opacity; 
}
.logo-html:hover{ 
    border-top-width: 3em; 
    border-bottom-width: 0em; 
}
.logo-html:hover + .logo-css{ 
    opacity: 1; 
}
.logo-svg{ 
    position: absolute; 
    width:0; 
}
</style>
<svg class="logo-svg" xmlns="http://www.w3.org/2000/svg">
    <filter id="svgThreshold">
        <feComponentTransfer>
        <feFuncR type="discrete" tableValues=" 0 1"/>
        <feFuncG type="discrete" tableValues=" 0 1"/>
        <feFuncB type="discrete" tableValues=" 0 1"/>
        </feComponentTransfer>
    </filter>
</svg>
</div>
