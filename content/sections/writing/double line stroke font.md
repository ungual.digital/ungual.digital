title: stroke fonts drawed out of CSS
tags: CSS, typography, stroke
date: 2023 06
summary: A word on stroke font on the web
thumbnail: /images/articles/cssstroke/overview.png

[TOC]

## A word on css-rendered stroke fonts

I took the opportunity of the [OSP member call](https://call.osp.kitchen) to experiment a long dreamt idea:
to draw the typography on a webpage by the ductus of a font, or main stroke, and not by its outline.

%%% figure-full
    ![a screenshot of the osp member call](/images/articles/cssstroke/call.png){: }
    
    %: A screenshot of the osp member call, showing two single-line fonts, in different sizes, with the same stroke width.

The main obstacle in this process is one of file format. For a real single-line font (a font, or shape without thickness or weight) the fontformat should support open, unclosed paths, virtually no fontformats supports those. A closed path forms a loop, e.g. the letter O. In a way the path doesn't have a begin- nor endpoint. An unclosed, or open path has extremities which don't join, e.g. the letter S.

It is possible to encode open paths in fonts using SVG. This can be done through SVG fonts, currently only supported in Safari, or, through the SVG table in an OpenType font, supported by Firefox, Safari and edge, but explicitely rejected by Chrome. However there may be hope in this track through emojii: 

> “The SVG font format — as a stand-alone font format — is essentially obsolete. More or less, no one is using stand-alone SVG fonts. However, SVG fonts are perhaps surprisingly alive, thanks to emoji. The SVG font format has actually been enshrined within OpenType, and OpenType fonts can contain an ‘SVG font table’, which is simply a font constructed according to the SVG 1.1 font format. Fonts of this spec are called ‘OTF+SVG’ fonts, and typically intend that when client software cannot display a given character in the SVG table, it will fall back and display a TTF alternative. The neat thing about OTF+SVG is that in addition to having stroke-based characters, it can also contain color information — and so this format is sometimes referred to as a ‘Color Font’. OTF+SVG is supported by Photoshop, Illustrator, Firefox, MS Edge, Pages, Keynote, TextEdit, and others. Because of this, SVG fonts do still exist, even though they aren’t often observed on their own.”
>
> &mdash; from [hershey-text-v30](https://www.evilmadscientist.com/2019/hershey-text-v30/)

This approach was explored by isdaT in their [SingleLine_otf-svgMaker](https://github.com/isdat-type/SingleLine_otf-svgMaker).

> “The resulting open paths OpenType-SVG fonts comprise two layers: a single-line layer based on SVG and a second layer with a classic outlined OTF. By default, if an application can’t use the open paths SVG layer it should switch automatically to the outlined OTF layer also embedded in this OpenType-SVG fonts.”

## the craft

What is happening here, is different. 

It uses a hack develped by people using stroke fonts for cnc and laser cutting machines, who have been exchanging online fonts made through this hack for years. Their trick is to transform open paths into **artificialy closed paths**. This is done by doubling the path, reversing the direction of the copy and then connecting the orginal path and its reversed copy. This results in a path where every point is doubled, except for the extremities. It allows to export a zero-thickness double-line font, which mostly behaves like a true single-line font because of the exact superposition of the two paths.

For this we used a [self-made python script](https://gitlab.constantvzw.org/osp/tools.slsvg2dlotf), using the [python fontforge module](https://fontforge.org/docs/scripting/python.html), to convert any **true single-line SVG font** to a **double-line OTF**, allowing an experimental use of “single-line” fonts on the web.

Fortunately, libre true single-line SVG fonts already exist, thanks to CNC uses. 
There is notably the inkscape extension **Hershey text**, for plotter drawings, which contains multiple variants of the **Hershey** fonts, used here. Other examples can be found on fablab forums and guides.

## css

Once the font is generated, the actual stroke is drawn in the browser itself with CSS using the [`-webkit-text-stroke`](https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-stroke) property.

This property is supported by most browsers, and included in the [Living standard](https://compat.spec.whatwg.org/#the-webkit-text-stroke), but not part of a CSS standard yet. This implies a great fragility, making it unusable for well-accessible webpages. Note that, more specifically:

* Chrome render square terminals and Firefox does rounded one, leaving the interpretation of the stroke itself to the browser.
* some letters may disappears, and some curves can appears as doubled at certain zoom level only, we have no idea why, but of course `-webkit-text-stroke` was never thought for that usage.

This particular approach has different nice consequences, that stroke font enjoyers may have dreamt to see possible on the web.

%%% figure-full
    ![a screenshot of the osp member call](/images/articles/cssstroke/one-line_0.png){: }
    
    %: A screenshot of the [self-made python script](https://gitlab.constantvzw.org/osp/tools.slsvg2dlotf) interface, showing different font and web-materialities with the same stroke-width

%%% figure-full
    ![a screenshot of the osp member call](/images/articles/cssstroke/one-line_1.png){: }
    
    %: A screenshot of the [self-made python script](https://gitlab.constantvzw.org/osp/tools.slsvg2dlotf) interface, showing different font and web-materialities with the same stroke-width

Because `-webkit-text-stroke` supports **global units** (meaning units independent to font-size), we can:

* make the same stroke width for text elements with different `font-size` (as the stroke width become independent from the font-size)
* make the same stroke width for different font-famillies
* make the same stroke width with other HTML or SVG elements (a border of a `div`, the outline of an SVG drawing, etc...)
* there already exist multiple libre font that can be processed for this use on the web: true single-lines SVG font from the libre CNC softwares worlds.

## fragility

This experiment could be seen as crossing multiple fields of interest of OSP, such as:

* The stroke versus outline typography paradigms
* CSS and the grain of the web mediums
* Frictions with open standards
* How tools shape practices

## web2plotter ?

This specific process could also be also to layout pen plotter drawing with `HTML` & `CSS`. If we export a `.pdf` from the browser with a double-line font drawed out of `CSS`, we could:

* open it in [Inkscape](https://inkscape.org/).
* use this [extension](http://cutlings.wasbo.net/inkscape-extension-removeduplicatelines/) to select all and remove all duplicate point.
* then use the Inkscape builtin extension that export to `HPGL` to a pen plotter.

With those kind of process, a markdown to penplotter could even work, allowing a fluidity and declarativeness in the process of designing for plotters.