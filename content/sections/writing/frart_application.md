title: Declarations proposal
date: 2023/01/06
summary: This text served as an application for a research proposal for FRART 2023
tags: css, declarative, poetry, theory
thumbnail: /images/articles/declarations/board_1.jpg

[TOC]

## Artistic research project overview

## Note summarising the project

**Declarations is a research into the poetic materiality of the CSS
web-standard (Cascading Style Sheets) and its echoes on design and
artistic practices. Declarations is a love letter to the crafts of
shaping with language.**

### Cascading Style Sheets

Cascading Style Sheets (CSS) is part of the ground matter of websites,
with HTML (HyperText Markup Language) and JavaScript. Though this
metaphor has its limits, we could say that HTML creates the objects, CSS
gives them shape, and JavaScript allows them to activate. CSS is both a
programming language and a standard, with designing the web being the
kernel of its practices. It tells to the apparatus of web-technology (a
browser[^1], a screen, a phone, etc) how the elements of a web-document
should present themselves and flow[^2]. The ways in which texts flow on
a screen, typographic choices, more-or-less griddy or flexible spaces,
hyperlinks and buttons appear clickable, unfolding menus, notifications
boxes and messages bubbles; but also colors, scrolling, animations and
responsiveness, all of them manifest themselves on our screens through
this layer of CSS.

From a web that was thought without fonts or colors, a web of documents,
slowly transforming into a web of apps and consumerism that revolves
around a few centralized platforms, the CSS standard has since been bent
in many directions. The traces of these transformations are hints on how
different actors have forced their point of view on what it means to
shape 'content' on the web. Today, everyone who interacts with
technology has to go through a layer of CSS: on every website, social
network or app. But CSS does not only live in the browser: it lives in
emails, pieces of software and operating systems. CSS is also not bound
to screens as it is more and more used to produce printed documents,
notably as an alternative publishing practice[^3]. Writing CSS has
become part of the job of many people: developers, designers, publishers
and websites amateur·ices. As more and more of our life happens on a
screen, design affects our lives and our narrations around technology,
as well as aesthetics and cultures. At it's core, **Declarations
re-thinks how the language of CSS shapes the relation to our
(technological) environment and investigates its narrations.**

### Poetic Material

CSS has an ambiguous nature: it's both a language and a material.
Unlike most programming languages it doesn't work around algorithms,
but by describing what should happen. By shaping through language, it
weaves intentions, narration and meaning into them, as well as it
creates visual cultures. **The research postulates that the relations
between language and design in CSS induces inherent poetics on the
material.**

> "I believe every material has a grain, including the web. But this
> assumption flies in the face of our expectations for technology. Too
> often, the internet is cast as a wide-open, infinitely malleable
> material."
>
> &mdash; Frank Chimero, [The Web's
> Grain](https://frankchimero.com/blog/2015/the-webs-grain/)
> (2015)

However, for practitioners in the field of design, its singularities as
a material are often taken for granted or forgotten as some kind of
technical or purely functional artifact that we have to accept. **The
research will use artistic gestures in order to talk about CSS ambiguous
nature and bring it somewhere else than from it's purely functional
technicalities.**

### Transdisciplinarity

The research is initiated and coordinated by *Doriane Timmermans*, and
working in togetherness with a variety of practices. As a daily CSS
writer from both digital art and graphic design, she has become intimate
with its materiality in different environments, each with their
respective technical, economical and aesthetical constraints. She is
part of *Open Source Publishing*, a graphic design collective that works
with open-source and libre softwares and investigates the affordances as
well as non-neutrality of digital tools.

From the different web-standards, the research decides to focus on CSS
from an observation: **seeing an ensemble of spread, alternative
gestures that revolve around CSS, in ways that deviate from the
standardized web and design industry, and where its role seems to escape
from a purely technical position.** Those diagonal CSS-based practices
gather internet artists, alternative designers, hacking developers, and
makers of handmade websites. However CSS is still too often approached
as a mean, putting its materiality and specificity aside, and those
experiment are often presented as "tricks" where it could be claimed
as a form of *crafts*.

> "In today's highly commercialized web of multinational corporations,
> proprietary applications, read-only devices, search algorithms,
> Content Management Systems, WYSIWYG editors, and digital publishers it
> becomes an increasingly radical act to hand-code and self-publish
> experimental web art and writing projects."
>
> &mdash; J.R. Carpenter, [A Handmade
> Web](http://luckysoap.com/statements/handmadeweb.html) (2015)

**Declarations aims to bring together a local network of CSS artisans,
by creating moments where practitioners can cross paths in a
transdisciplinary artistic environment.** Transdisciplinarity in the
research is both a political and social choice as it is a mean to
constantly shift our views. Declarations starts a process of
**documenting and collectivizing knowledge on the alternative crafts of
CSS**.

Declarations invites the expertise of [Open Source
Publishing](http://osp.kitchen/) and
[Varia](http://varia.zone/). Within the project they'll
research collaborative digital tools and spaces, initiate conversations,
and start forming a larger network of CSS practices.

## Artistic note to develop the proposal and its major axes of inquiry

Through dedicated research time for the research coordinator and two
worksessions, the project will investigate theoretical questions, and
experiment artistic gestures, in order to form a corpus of material on
CSS poetics and politics. The research will revolves around the
following axes and questions, influenced by political and situated
motivations detailed here.

### Language & materiality

CSS has an ambivalence as a material and a language. The fact that CSS
is text-based fundamentally differentiates it from other design
paradigms that imposed themselves as the normative way to publish with
software[^4]. In CSS, there is no fixed canvas onto which we visually
compose, no tools we can select from a toolbar, no other files to import: 
we no longer place elements with gesture but with words. A design
becomes as much about its visual implications rendered by the browser as
it is about the text that declares it. This particular paradigm of the
web hybridized design practices, extending to a vastness of publishing
practices.

> "My favorite aspect of websites is their duality: they're both
> subject and object at once. In other words, a website creator becomes
> both author and architect simultaneously."
>
> &mdash; Laurel Schwultz, [My website is a shifting house next to a river
> of knowledge. What could yours
> be?](https://thecreativeindependent.com/essays/laurel-schwulst-my-website-is-a-shifting-house-next-to-a-river-of-knowledge-what-could-yours-be/)
> (2018)

The research proposes to investigate **how designing with language has
hybridized design practices. It asks for new methods to approach CSS
that acknowledge its duality as a literary and visual material**, or as
*Laurel Schwultz* puts it how it makes us *"both author and architect
simultaneously"*.

As it by uses a set of defined and limited English (key)words taken from
the common language (and numerical values), it can feel both rigid and
meaningful. An example can be shown by the following CSS line:
"position: relative;". To say that the position of an element is
relative to another can become a way to reflect at how we situate thing
in relation to one another. In the same way we can think about the
following CSS properties - casually parts of every websites - as symbols
to be activated: background, display, border or transition.

%%% figure
    ![]({static}/images/articles/declarations/dk-film.png)
    ![]({static}/images/articles/declarations/dk-film-inspect.png)

    %: *Dina Kelberman* - [Web Poems](https://dinakelberman.com/filmpoem/) is a
    series of artworks that are visible in two places at once, the visual
    web-elements transforms themselves as does the code of the page, using
    custom HTML tags as narrative elements.

### Declarativeness (unfold CSS techno-poetics)

CSS is a declarative programming language, meaning it "*expresses the
logic of a computation without describing its control flow"*.
Programming languages that we most often use and quote aren't
declarative, but imperative: we precise explicit steps for the program
to execute, creating algorithms - like a recipe. Declarative programming
doesn't work by precising the how, but by declaring (or describing) the
outcome directly - more like the description of a meal. So in CSS,
we're not building sequences of actions, we're declaring direct
intentions. Every sentences of CSS, technically a couple of a property
and a value inside of a selector, is called a *declaration*. The
research investigates how, **similarly to the choice of words we decide
to use to tell a story, declarations in CSS can speak about our
intentions, and encodes narrations into the things we make?**

In her video essay [Why Is CSS So
Weird?](https://www.youtube.com/watch?v=aHUtMbJw8iA&abchannel=MozillaDeveloper)
(2019), *Miriam Suzanne* presents how this declarativeness is specific
to the complex technology that is the web. From the start of the
internet, websites have been thought as something that could be accessed
through a multiplicity of interfaces: on a desktop computer, on a phone,
a smart watch, and devices from the past or yet to be imagined. Moreover
than the obvious change in screen formats, they have different
web-browsers, colors, fonts; and have to deal with constraints like
available bandwidth, user customizations and alternative accessibility
interface like screen readers. This caused a big problem: how can we
design for *"a multitude of unknown canvases"*. CSS answers that we have
to *"accept to give up control"*.

> "The control which designers know in the print medium, and often
> desire in the web medium, is simply a function of the limitation of
> the printed page. We should embrace the fact that the web doesn't have
> the same constraints, and design for this flexibility."
>
> &mdash; John Allsopp, [A Dao of Web
> Design](https://alistapart.com/article/dao/) (2000)

Declarativeness opens a new door because unlike imperative programming
**descriptive language allows for a fluidity of interpretations.** We
can use this fluidity to adapt to its context of execution, taddressing
all the multiple canvases at once, knowing that we can't decide the
final result with certainty. Unlike the printed design industry, the web
had to let go the idea of a fixed image and make fluidity part of its
fundamental thinking. CSS as a material, is a like a clay that is for
ever wet, never baked. ***Declarations* think of declarativeness as
something fundamental yet complex about the way we dialogue with
technologies.** Declarations want to dive into what could be
*Declarative art*, a form of digital artistic practices of its own that
(could) already exists in the margins, distinctively from *Algorithmic
art*.

%%% figure
    ![]({static}/images/articles/declarations/hockney4.jpg)
    ![]({static}/images/articles/declarations/devicesizes.gif)

    %: Images taken from: *Frank Chimero*, [The Web's Grain](https://frankchimero.com/blog/2015/the-webs-grain/) (2015).
    First: *The Scrabble Game*, by *David Hockney*, 1982, as a metaphor of
    the broken space-time fluidity of designing for the web.
    Second:
    *Spectrum of Android Fragmentation sizes* from *OpenSignal*, 2012,
    various screen formats that can welcome a webpage, overlayed on top of
    each others.

### Standards, cultures & power dynamics

The edges of CSS as a (programming) language are blurry. CSS is a
standard maintained by the W3C (the World Wide Web Consortium)[^5]. At
its core, CSS is a set of recommendations[^6], suggesting how things
should be interpreted. Coming from dreams of accessibility and openness,
it leaves the freedom to each software to follow or deviate from the
standard. By having these choices, browsers vendors hold a political
power: to bend the language we use to build the web ecosystem. History
-- notably the *"the browser wars" -* showed that there are divergences
about which logic should dictate its evolution. Preferences of default
styling, properties considered superfluous and thus removed, or
unofficial ones forced in the standard. Knowing whether it makes sense
to use a properties has became its own science, culminating in the
well-named website [Can I use](https://caniuse.com/).
***Declarations* aspires to learn from the historical divergences of
opinions on the CSS standard, and see how artistic gestures could enter
in dialogue with its evolution.**

As with every standard, it would be naive to ignore the power dynamics
that are at play here. While taking space on the web progressively
became a synonym of power, corporations redefined the environment of the
web, notably through CSS. It is then no surprise that actors like
browser vendors, the GAFAM, and Adobe are particularly interested in
directing its evolution. The very recent acquisition of Figma[^7] by
Adobe for the unthinkable sum of $20bn is only a hint at how owning the
means of webstandards is a source of power. Technical decisions lead to
cultural impact. We can only imagine how the choices of what makes it to
the CSS standard could have redefined our attention to various visual
elements, its impacts on our lives expanding far outside of the screens
space. **Therefore it seems fundamental for Declarations to ask what are
the power dynamics, as well cultural positioning, at play in the CSS
standard? Who are the actors of this political ecosystem and what are
their motivations?**

### Meaningfulness (design as an articulation)

Before CSS came out, web styling was a part of HTML, leading to a rather
wild reality of compatibility problems. In 1996, CSS was proposed from
an effort of standardization through its own interesting paradigm: the
desire of a split, allegedly separating the "content" from the "style"
by putting them into distinctive files, namely HTML and CSS. A binary
distinction between the object and its shape that left a definitive
impacts on web-design practices, compartmentalizing designers and
developers by restraining them to different files.

> "But what are we supposed to separate exactly? From the meticulous
> documentation of the discussions that led to the development of CSS,
> it seems that not much time has been spent on discussing the choice of
> the word pairs 'substance' versus 'form', their later equivalent
> 'content' and 'style', or even more outrageous, 'meaning' and
> 'presentation'. The ease with which the various working groups are
> able to put such porous concepts to use as binary oppositions, is not
> surprising coming out of the bureaucratic culture of the W3C."
>
> &mdash; Femke Snelting, [Dividing and
> Sharing](https://snelting.domainepublic.net/texts/divideshare.pdf)
> (2008).

To say that CSS is about presentation would be a suffering limitation.
As *Femke Snelting* suggests it, *"webdesign is a work of
articulation"*. By giving specific shapes and behaviors to web-elements,
CSS articulate meanings. Uses speaks about intentions, and by repeating
and spreading them, it creates visual cultures. **Declarations argues
that CSS, as a material that shapes (the web), has an deeper than skin
impact and that it articulates meaning**. **Declarations emphasizes the
necessity to to overcome the binary limitation of the industry, and to
decompartimentalize practices.**

%%% figure
    ![]({static}/images/articles/declarations/cfl-2018-11.png)
    ![]({static}/images/articles/declarations/coding-from-life-2018-yale-zoomin.jpg)

    %: *Laurel Schwultz* - [Coding from Life](https://veryinteractive.net/pages/coding-from-life.html), an exercice at Yale 2018 that consist on making still life drawing using only HTML and CSS. Credits for first image unknown.

%%% figure
    ![]({static}/images/articles/declarations/pc-corentin-deschamps.jpg)
    ![]({static}/images/articles/declarations/pc-corentin-deschamps-css.png)

    %: *Romain Marula* teaches digital culture to painting students at le 75,
    he asks them to make a painting and then redo it using CSS, becoming
    both a description and representation of the original painting. This one
    was made by *Corentin Deschamps*.


### Gesture (slowness and craft)

On the industrial side, CSS has been over-standardized and packaged into
templates, frameworks and utilities, creating generations of look-alike
websites, and ready-to-use services. Lots of them, like *Wordpress* or
*Squarespace*, use the fact that you don't have to code as a commercial
strategy. It's suggesting that design has already been solved for you
by their experts and packaged into some CSS files, removing all senses
of agency we can have in participating to writing the web. But the
alienation caused by mainstream online spaces is more and more
acknowledged, and desire to come back to some forms of independence and
simplicity has grow.

> "I want our personalities to come through not just in the words or
> links we share, but in the URLS we use and the code we write."
>
> &mdash; Zach Mandeville, [Basic HTML Competency Is the New Punk Folk
> Explosion!](https://coolguy.website/basic-html-competency-is-the-new-punk-folk-explosion/)

There are movements of handmade websites that cultivate processes of
*"slowness and smallness as a form of resistance"*[^8]*,* handcrafting
websites as we would build furniture for our room instead of buying them
at IKEA*.* Libre and open-sources designers have been investigating the
potential of HTML & CSS to produce and publish printed documents, as an
open standard alternative to the hegemonic approach of Adobe softwares.
Artists have also been using HTML & CSS as a poetic medium since the
early days of the internet, notably in Net Art. But today this
preoccupation goes beyond a specific art movement: by being an artist
with a website you also become the authors of you own space, creating
ways of telling a practice through web-standards. Sometimes out of
necessity, sometimes out of curiosity, those contexts gave CSS the
opportunity to be written closer to its facture, creating visual
cultures and folklores.

Because of its complex history of exceptions, writing CSS can requires
precise knowledge. By taking alternative paths to the standardized one,
web artisans dived into the vastness of its fragile possibilities. Those
crafts are often poorly documented or put aside as technicalities.
Moreover the occasions discuss CSS uses are constrained: either by
discipline (already suffering from the designer versus developer
separation), or by the format (highly limited to teaching design or
online code tutorials). **Declarations creates moment for the
multiplicity of CSS practices to meet and approach it like crafts.
Through sharing stories and writing CSS in togetherness, it starts a
process of documenting, and collectivizing knowledge, and make the
fragile voices of CSS artisans heard.**

## Development and chronology of the proposal

Within the framework of FRArt, Declarations includes two
**workessions**, bringing together a local network of CSS practitioners.
They form an input for the research coordinator to collect and
investigate example of friction and craft on CSS. Each of the
worksessions will be followed by a respective **public window** that
serves to make those experimentation echoes in public open formats. It
will also launch a **collaboration with** [Open Source
Publishing](http://osp.kitchen/) (Bruxelles based) and
[Varia](http://varia.zone/) (Rotterdam based), through an
initiation phase that focuses more on the *how* of the research.

### Initiation phase

*OSP* and *Varia* are sollicitated for their expertise as hybrid
artistic and designers collectives and their knowledge in collaborative
practices. **They will create web tools and spaces that can act as
companions for practicing the research,** coming up with alternative
methods to explore CSS collaboratively, as such methods have yet to be
developed. Those will be hosted online to become a platform:

* as canvas for the worksessions to write and experiment with CSS
collaboratively
* as a support of documentation to the stories and craft of CSS (as the
web-tools-spaces are themselves made of CSS, it acts both as platform
and a form for the research)
* to publicly share the outcomes and process of the research online.

It will also activate an inter-locality network, and help diffusing
calls. Those tools can later be updated in-between worksessions.

This phase also includes a research into **worksession formats and
methodologies** that are inspired by different cultures of doing: the
ones of collaborative designers and from collaborative writing as a
literary process.

### Worksessions

**The worksessions core is to invite CSS practitioners to share a story
about their crafts and experiences with CSS.** Participation will be
partly on invitation, partly through open call, to ensure a diversity of
practitioners and focus on the locality of Brussels. Taking part in the
worksession you join the network by bringing in your own problematics.
Worksessions include both moments of sharing stories and of
experimentation. The knowledge on singular CSS uses will acts as a
starting point to experiment collectively with the companion tools.

Even though CSS mostly happens on our screens, these sessions aim to be
performed in the intimacy of a common physical space, **a step away from
our individual and non-oralised relation to technology**. Discussing the
language of CSS aloud can become a process to perform its linguistic
nature and declarativeness. The places hosting the workessions have a
role in **relocating CSS into a transdisciplinary artistic context**.
Considered places are such as: La Balsamine, Meyboom-Artist-Run-Spaces,
La Maison du Livre, la Bellone and local bookshops.

### Public windows

After each worksession, the local participants are commissioned to
co-create a public moment in various potential forms: in between
online-art, installations, publishing and performances. They offer a new
context to the stories initially shared by the invited participants to
become something new. Those public moments aims to question local
designers and artists on the current investigation of the research.
Those events are seen as both of technical and poetical nature, and one
of the dreamed achievement is to be able to weave with both at the same
time.

One public window has been planned in collaboration with
[Constant](https://constantvzw.org/), active in the fields of
art, media and technology which includes (cyber)-feminism,
transdisciplinarity and libre and open source software.

%%% figure
    ![]({static}/images/articles/declarations/osp-upanddown.png)

    %: Open Source Publishing - [Up Pen Down](http://osp.kitchen/live/up-pen-down/), 
    *"This performance
    was the first public moment of a research focused on that which is
    between digital type design and bodies. Letters and movements, dance
    notation and programming, digital codes and coded physical gestures,
    plotters and body parts interacted with each other and blurred the
    distinction between choreographic and digital practices."*

%%% figure
    ![]({static}/images/articles/declarations/othertime.png)

    %: Raphael Bastide -
    [https://otherti.me/other/box/](https://otherti.me/other/box/),
    *"Recording of a friend describing a box, a CSS animation tries to
    follow its description."* The screenshot has been taken when the
    narrator says *"it should be somewhat visible to the person that grabs
    the box or put something inside can understand how the box was made".*

### Solidifying

Time is dedicated in between events for the research coordinator to
documents the worksessions and public windows. **With those inputs she
creates a corpus of shared stories and deepens the initial questions of
the research.** It includes the writing of the activity report, and the
publication of certain experiments online. Another preoccupation is to
keep the local network active, and see how it can continue its dialogue
with standard politics and artistic gestures, notably through the
platform.

The author and artist *An Mertens*, ex-member of Constant and active
member of Algolit, solicited for her expertise in coordinating long-term
research and her knowledge of cultural sector related to digital
culture, will be an occasional soundboard, advising the research
coordinator through recurrent meetings.

At the end of the research a workshop will be organized with the
teachers of erg to see how the results of the research, developed
methodologies and tools can be used in school curriculum.

### Preminilary calendar

The calendar is subject to changes, and also does not includes all the
researcher's works on deepening the research tracks and coordination of
research. All mentioned collaborators have been contacted in the
framework of this application, and are informed of the calendar.

**June 2023, Initiation:**

* researching worksesion formats and methodologies
* tool/space thinking/developing with OSP and Varia

**September 2023, Initiation:**

* tool/space thinking/developing with OSP and Varia
* organisation of the research tracks and resources

**November 2023:**

* coordination of worksession 1 (thematic, invitations, calls)

**January 2024, worksession 1** (4 days)

**February 2024, solidifying:**

* documentation , and reflection on the research axis
* public window 1 creation with the local practitioner
* collective tools/spaces updating
* research coordinator work on activity report

**March 2024, public window 1** (at Constant)

**May 2024:**

* coordination of worksession 2 (thematic, invitations, calls)

**September 2024, worksession 2** (4 days)

**October 2024, solidifying:**

* documentation , and reflection on the research axis
* public window 1 creation with the local practitioner
* collective tools/spaces updating

**November 2024, public window 2** (to be defined)

**December 2024, closing chapter**

* research coordinator work on activity report
* reflection on to continue the local network in dialogue

Time for the workshop at erg is yet to be defined.


### List of potential invites

**This is kept private**

<!--
* Laurel Schwulst (as a **teacher**) - [coding from
life](https://veryinteractive.net/pages/coding-from-life.html), on
a poetic CSS still lives exercise.
* Olia Lialina (as a **web archivist**) - [One terabyte of kilobyte
age](https://anthology.rhizome.org/one-terabyte-of-kilobyte-age),
on a gigantic archive of removed geocities websites from the early web.
* Miriam Suzanne (as a **member of the W3C CSS** working group and as a
**theater writer**) -
[miriamsuzanne.com/speaking/](https://www.miriamsuzanne.com/speaking/)
and
[miriamsuzanne.com/theater/](https://www.miriamsuzanne.com/theater/).
* Raphael Bastide (as a **CSS musician**) -
[Cascade](https://www.miriamsuzanne.com/theater/), on turning CSS
into sounds in live-coding.
* Marie Otsuka and Roel Roscam Abbing (as **designers with
ecology/nomy**) -
[solar.lowtechmagazine.com](https://solar.lowtechmagazine.com/about.html#how),
on a website served by solar pannel, designed with radically minimalist
CSS.
* OSP (as **investigators of speculative CSS trajectories**) -
[osp.kitchen](http://osp.kitchen/research/) on working with CSS
Regions a property removed from the standard.
* Romain Marula (as a **CSS painter**) - [Painting
Club](http://ivro.fr/#painting-club), on teaching painting
students to compose with CSS.
* Julie blanc (as **a web2print archivist**) - [la bibliothèque web to
print](http://2print.org/), on creating a library of book made of
CSS.
* Constraint Systems (as **a tool maker**) -
[cascade.constraint.systems](https://cascade.constraint.systems/),
on making poetic CSS based tools.
* Everest pipkin (as **a poet**) - [Soft
Corruptor](http://cordite.org.au/poetry/game/soft-corruptor/), on
a poem about being able to inspect, and being able to unfold.
* Zach Mandeville (as a **zinemaker**) -
[coolguy.website/web-zine/01/](https://coolguy.website/web-zine/01/),
on a radically simple web zine.
-->

## The reasons for collaborating with the Facilitator (esa)

As a previous student of the school ERG, the research coordinator
continues to construct both a social and ideological link with the
school and its community. This particular link definitely create a
solidity in the possible interactions between Declarations and ERG. As a
member of OSP, and an active person in open-source and libre design
communities of Bruxelles -- an approach particularly emphasized in
design teaching at ERG, she is often in contact with teachers of the
school. The bringing of Declarations local network is closely related
and intersects with the students and teachers of the school.

She had many occasions to see how the pedagogy at ERG is rooted in
transdisciplinarity, often blurring the distinction between design and
art practices, digital native or non-digital native, poetics and
techniques. As one of the methods of Declarations is to
decompartmentalize practices this confirm a strong ideological
parallelism of methods, that hopefully could feed each other.

Of course Declarations pedagogic dimension is important: as it aims to
elaborate a new angle of approach on the digital materiality of CSS, and
with it, constructing methods, resources and tools. All those could be
used as pedagogical material as CSS is already taught with different
approach, and by entering in dialogue to echo gracefully within the
pedagogical practices of the school. Declarations has the particularity
to address artistic and linguistic problematic, hoping to create
interest in non-design students for those web-related problematics.

## Selection of the 5 most significant productions and/or artistic research of the author

**This is kept private**

<!-- *Please provide a maximum of 5 productions and/or artistic research
titles.*

*In order to support your application, it is required that you prepare a
portfolio of max. 100Mb that includes those 5 most representative
productions and/or artistic research works (links, photos, verbatim).*

* *Elements by Element*, Doriane Timmermans, 2020
* *moonwalks.be*, Doriane Timmermans, 2022
* *pseudo force*, Doriane Timmermans, 2022
* *Actual Art Tips by Actual Artists*, Doriane Timmermans, 2019
* *Balsamine 2021-2022*, Open Source Publishing, 2021 -->



<!-- footnotes -->

[^1]: A browser or web browser is one of the most common interfaces to
    access websites, the most used are Chrome, Safari and Firefox, They
    are not only a door but also the engine interpreting the code of the
    website, interpreting the HTML, CSS and Javascript to render the
    website as it is requested.

[^2]: CSS is not « autonomous » as it applies on web-elements created
    through other languages. However it is not limited to HTML, and is
    used to describe the expressions of other markup languages like SVG
    (Scalable Vector Graphics) and even non-visual one, like the volumes
    and interactivity of sounds.

[^3]: Making printable documents by using web-standard, sometimes
    referred as « web2print », is being more and more developed by
    alternative and open-source designer communities (notably by
    PrePostPrint, Open Source Publishing and paged.js) and teached as an
    alternative publishing practice through workshop and cursus in
    schools.

[^4]: As wikipedia says *"Desktop publishing (DTP) is the creation of
    documents using page layout software on a personal computer."*
    Editorial practices have been heavily influenced by DTP softwares.
    Most of them being WYSIWYG (What You See Is What You Get), meaning
    the systems through which you edit the content is the same systems
    that displays the result; which is the case for inDesign by Adobe,
    but is not the case when working with CSS. Because of both the
    history of design and the widespread use of Adobe, some specifics
    ways of thinking/doing about layout, editions and typography have
    been culturally implemented as norms, and to divert from it often
    ask for a particular willingness.

[^5]: "*The work of the W3C is financed through contributions of member
    organisations; software vendors, universities and telecom companies
    pay up to 65.000 dollars (41,600 euros) per year out of interest in
    codefining the way the web can be made (inter)operable."* - from
    [http://www.w3.org/Consortium/](http://www.w3.org/Consortium/)

[^6]: *"The CSS Working Group creates documents called specifications.
    When a specification has been discussed and officially ratified by
    W3C members, it becomes a recommendation. These ratified
    specifications are called recommendations because the W3C has no
    control over the actual implementation of the language. Independent
    companies and organizations create that software. The World Wide Web
    Consortium or W3C is a group that makes recommendations about how
    the Internet works and how it should evolve." -* from
    [https://www.tutorialspoint.com/Difference-between-specification-and-recommendation-that-introduced-CSS](https://www.tutorialspoint.com/Difference-between-specification-and-recommendation-that-introduced-CSS)

[^7]: Figma is a web application that allows designers to prototype and
    design websites and interfaces in a collaborative manner. It is
    widely used in the web-design industry, though working with it is a
    specific approach that was also criticized.

[^8]: The term "handmade web" was coined by J.R. Carpenter to describe a
    relationship to crafting website. She characterizes this
    relationship both as an opposition to industrial (*"individuals
    rather than businesses or corporations"*), and to emphasize their
    object-like materialities - shaped by hands, as situated
    constructions and artifacts. This relationship forms a resistance
    against a capitalistic data-oriented web.
