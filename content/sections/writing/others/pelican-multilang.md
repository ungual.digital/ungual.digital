title: A folders based approach to multilang website with Pelican
tags: Pelican, python, language, i18n, folders, static, relativeness
date: 2022 08 16
status: draft

[TOC]

## Context

First let's state what are the default behaviors and possibilities in term of making multilang websites with Pelican.

By default, Pelican does not make multilang website but it does handle translation of articles.
Meaning the webpages of the website (each corresponding to a markdown article to make it simple), can have any number of _translations_, this can be presented as a series of links on the _original_ version of the webpage to their _translations_.
This behavior means a few things:

* **direct templates** are not translated (page that are not a markdown article, like the index.html, or others sorts of synthetic or specials pages).
* **partials** (recurrent elements on different page, like the navigation menu) are not translated.
* **navigation** doesn't remember language, you browse the website in the `DEFAULT_LANG`.
* in terms of **paths/urls**, the website is not split in different parts for every languages, but has one part where some webpages can have translation (appending _lang to the filename for example). 

Another default of Pelican is the way it detects article in the content are translations of each other.
It does that by looking at two metadatas: `lang` and `slug`. `lang` has to be defined in the frontmatter of every articles, saying this article is in french _"manually"_. The `slug` is used to detect translations: two articles with same `slug` and different `lang` are translation of each others.
However, because `slug` is by default constructed using the `title` of the article, and that the `title` of an article will often change if it's in another language it also asks us, for translated articles, to _"manually"_ define a equivalent `slug` for a group of translated + orginal articles.

## Translations detection by folder placement

What we want to do, is to identify translated articles if they have the same relative path to a specific `lang` folder, without having to declare any metadata in the files themselves (like `lang` or `slug`). 
And because Pelican is well designed for those light hack it's perfecty possible!

Say we have a `content/` structured like this
```
├── fr/
│   └── event/
│       └── 2022/
│           ├── article_1.md
│           └── article_2.md
└── en/
    └── event/
        └── 2022/
            ├── article_1.md
            └── article_2.md
```

The first thing is to extract the `lang` metadata from the `source_path` of the article, instead of having to write it. 
Pelican has the `PATH_METADATA` that can be used exactly for that, using a _regex_ and _python capture groups_ with the `(P<name>)` syntax (see in the doc: <https://docs.getpelican.com/en/latest/settings.html#metadata>).

```python
# a regex that extract any metadata from the whole source_path
# first folder is lang metadata, others are category, then the filename
PATH_METADATA = '^(?P<lang>.+?)\/(?P<category>.*)\/.*'
```

Note that here we are capturing one more metadata, saying that the rest of the path (i.e. `event/2022/` following the above example) is the category. A nice trick there is that by using the [_more-categories_](https://github.com/pelican-plugins/more-categories) plugin, we can spit that in a sequence of nested sub-categories, that can be used later in the template.

Now to make Pelican actually detect translation is a bit more tricky.
There is a `ARTICLE_TRANSLATION_ID` and a `PAGE_TRANSLATION_ID` that allow a collection of metadata name to identify translation.
We could want to do this then `ARTICLE_TRANSLATION_ID = [category, basename]`, unfortunately Pelican doesn't allow to use the filename in the `TRANSLATION_ID` collection. But by extracting it with `PATH_METADATA`, we can!

```python
# a regex that extract any metadata from the whole source_path
# first folder is lang metadata, others are category, category+filename is the translation identificator
PATH_METADATA = '^(?P<lang>.+?)\/(?P<transid>(?P<category>.*)\/.*)*'

ARTICLE_TRANSLATION_ID = ['transid']
PAGE_TRANSLATION_ID = ['transid']
```

And that's it! We don't need to write any metadata manually to handle translations anymore, just put it in the right folder.

I often think that you can already extract a lot of information **from where you put things relatively from each others** that sometimes it's not needed to **having to say it again inside the things themselves**, this is why i personnaly like this approach.

It also a lot easier to understand from an exterior point of view, if I have to explain to the people editing the article they have to put a `lang` metadata and a make sure the `slug` metadata correspond, instead of explaining them to put the french article in the `fr/` folder.

## i18n subsites

Secondly, we can use the [_i18n-subsites_](https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites) plugin, in order to **actually create a multilang website**. Also if, like me you didn't knew it already, and i know how it can be scary to look at thing you feel you don't understand just because you don't understand the name of it, so *i18n* just mean _internationalization_, the _18_ is just the number of letters between the _i_ and _n_.

First, let's look at what the plugin actually does:

1. generate the main site (like the plugin is not there) in `DEFAULT_LANG`, keeping the articles not in this lang as _translations_.
2. for every subsites declared, it switches `DEFAULT_LANG` to the lang of the subsites, and generate a subsite by default by appending the `lang/` to the `SITEURL`, meaning _translations_ will become the _originals_ alternatively for every subsites.

It asks to define a `I18N_SUBSITES` dictionnary settings, with an entry for every subsites.

Note that it is still important to detect which article are translations with the process explained in the previous section. If we wouldn't do that, we could have article in a different lang from `DEFAULT_LANG` to be considered as _originals_ because they are not identified with any other article as their _translations_, meaning they will appear in different subsites.

### Sub-sites structure

In my case i want to end up with an output similar to the content, meaning something like
```
.
├── theme/
│   ├── css/
│   │   └── stylesheet.css
│   └── js/
│       └── script.js
├── images/
│   ├── image_1.jpg
│   └── image_2.jpg
├── fr/
│   ├── index.html
│   ├── calendrier.html
│   ├── event/
│   │   └── 2022/
│   │       ├── premier_article.html
│   │       └── deuxième_article.html
│   └── pages/
│       └── a_propos.html
└── en/
    ├── index.html
    ├── agenda.html
    ├── event/
    │   └── 2022/
    │       ├── first_article.html
    │       └── second_article.html
    └── pages/
        └── about.html
```

Note here that the filename (and thus the ends of urls) are translated in the language we're in, this is because `SLUGIFY_SOURCE = title` setting is there by default, and title of article have been translated. The direct templates filename have also been translated! (like _calendrier_ and _agenda_) I will explain this later.

Beside of the `STATIC` content we don't want to generate the main site, meaning we don't want to have an `event/` or `pages/` folder at the root, we only want them in the websites. So we have to remove the article and page generation for the main website, and redefine them for the sub-sites. Here is an example according to the above structure.

```python
DIRECT_TEMPLATES = ['index', 'agenda']

# do not generate main site (article and pages)
ARTICLE_SAVE_AS = ARTICLE_LANG_SAVE_AS = ''
ARTICLE_URL = ARTICLE_LANG_URL = ''
PAGE_SAVE_AS = PAGE_LANG_SAVE_AS = ''
PAGE_URL = PAGE_LANG_URL = ''

# do not generate direct templates
INDEX_SAVE_AS = ''
AGENDA_SAVE_AS = ''

I18N_SUBSITES = {
    'en': {
        'SITENAME': 'website (en)',
        'SITEURL': 'en/',

        # article and pages
        'ARTICLE_SAVE_AS': '{category}/{slug}.html',
        'ARTICLE_URL': '/{lang}/{category}/{slug}.html',
        'PAGE_SAVE_AS' :'pages/{slug}.html',
        'PAGE_URL' : '/{lang}/pages/{slug}.html',

        # direct templates
        'INDEX_SAVE_AS' : 'index.html',
        'AGENDA_SAVE_AS' : 'agenda.html'
    },
    'fr': {
        'SITENAME': 'website (fr)',
        'SITEURL': 'fr/',
        
        # article and pages
        'ARTICLE_SAVE_AS': '{category}/{slug}.html',
        'ARTICLE_URL': '/{lang}/{category}/{slug}.html',
        'PAGE_SAVE_AS' :'pages/{slug}.html',
        'PAGE_URL' : '/{lang}/pages/{slug}.html',

        # direct templates
        'INDEX_SAVE_AS' : 'index.html',
        'AGENDA_SAVE_AS' : 'calendrier.html'
    }
}
```

Two things to note here:

1. we add the `lang` metadata of articles and pages to their url, so when we click on a link in a subsite we stay in this subsite. But the `SAVE_AS` is relative to the `SITEURL` so we don't have to add it.
2. We created a new setting `AGENDA_SAVE_AS` to be able to rename the additionnal direct template according to language, if you didn't know in this way you can control direct template like page or article.

### Navigation

With this we get the structure defined above. But we still need to do one thing: the navigation.

For that we can use the `MENUITEMS` Pelican setting and create a navigation menu template by iterating on it. Every entries of `MENUITEMS` is a tuple of a name and an url, and like the other settings, it can be redefined for every subsites.
This mean we can do this (omiting the lines of code about pages and articles)

```python
DIRECT_TEMPLATES = ['index', 'agenda']

# do not generate direct templates
INDEX_SAVE_AS = ''
AGENDA_SAVE_AS = ''

agenda = {
    fr: 'agenda' 
    en: 'calendrier'
}

I18N_SUBSITES = {
    'en': {
        # direct templates
        'INDEX_SAVE_AS' : 'index.html',
        'AGENDA_SAVE_AS' : agenda['en']+'.html'

        # menu items
        'MENUITEMS' : [
            ('home', ''),
            (agenda['en'] , agenda['en']+'.html')
        ]
    },
    'fr': {   
        # direct templates
        'INDEX_SAVE_AS' : 'index.html',
        'AGENDA_SAVE_AS' : agenda['fr']+'.html'

        # menu items
        'MENUITEMS' : [
            ('acceuil', ''),
            (agenda['fr'] , agenda['fr']+'.html')
        ]
    }
}
```

Note that:

* when using the `MENUITEMS` list in the navigation menu template, we do need to preppend `/{{DEFAULT_LANG}}/` to the urls of every links. So the _acceuil_ link actually link to `/fr/` and the _calendrier_ link actually link to `/fr/calendrier.html`
* because both the `MENUITEMS` name and url, and the `SAVE_AS` of direc templates are linked, it makes sense to define the wording in a separate dict above. This could also be used to change the wording of the homepage!

### Multilang wording in `content/`

If we really want, we can still go a bit further by making the wording editable from the `content/` folder. This make sense if the editors do not have access to the `pelicanconf.py` file.

By using the [_data-files_](https://github.com/LucasVanHaaren/pelican-data-files) plugin, we can have a `data/` directory that contains either `yaml` or `json` files. Our data file can be a for example a `config.yaml` dictionnary, with the first entries being the lang and then the wording. This mean that we can use `DEFAULT_LANG` in the templates to request content in this dictionnary. 

So we can both:

* `import` the file in the `pelicanconf.py` for the wording of direct template `SAVE_AS` and `MENUITEMS`, 
* access it in the templates themselves, for example by changing the `<title>{{super()}}{{DATA_CONFIG[DEFAULT_LANG]['agenda']}}</title>` tag accordingly, to have either _agenda_ or _calendrier_ in the browser tabs title depending on which subsite we're in.