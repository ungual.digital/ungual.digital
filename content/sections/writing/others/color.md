title: colors and colors on the web
status: draft

# colors and  on the web

## Ideas

* naming is seeing
* colors spaces as math constructs are higly assymetrical things, and every colors as a particular places inducing unique properties. it's not homogenous at all (either on screen, on print, in your eyes cone, in frequency, etc)
* it's cultural and social construct, speaking about colors is speaking about your culture and history
* a lot of it is perceptual trickery, like brown that is dark orange with another words for it, or purple that is constructed in our brain but has no proper frequency, or rgb screen that are only emitting rgb frequency to create the illusion of continous color, etc.

## Percieving colors

## Non-Technological

Color studies
* <https://www.youtube.com/watch?v=gMqZR3pqMjg&ab_channel=Vox>
* <https://en.wikipedia.org/wiki/Linguistic_relativity_and_the_color_naming_debate>

## Technological

RGB screens
* <https://www.youtube.com/watch?v=uYbdx4I7STg&ab_channel=TechnologyConnections>

Compression
* <https://www.youtube.com/watch?v=h9j89L8eQQk&ab_channel=TomScott>

## Particular Stories

Unicode Heart color space
<https://www.unicode.org/L2/L2021/21075-heart-emoji-coverage.pdf>

Brown: the weirder color
* <https://www.youtube.com/watch?v=wh4aWZRtTwU&ab_channel=TechnologyConnections>

## HTML colors

history on web colors
* <https://en.wikipedia.org/wiki/Web_colors>

specification
* <https://www.w3.org/wiki/CSS/Properties/color/keywords>

naming digital colors
* <https://en.wikipedia.org/wiki/X11_color_names>
* <https://www.seximal.net/colors>
* <https://xkcd.com/color/rgb/>

tools on web colors
* <https://www.w3schools.com/colors/colors_converter.asp>
* <https://www.w3schools.com/colors/colors_groups.asp>
* <https://raphaelbastide.com/salmon-olive/>

art on web colors
* <https://rhizome.org/editorial/2020/jan/06/laurel-schwulsts-decade-in-internet/>
* <https://colorchat.soft.works/>, <https://www.youtube.com/watch?v=csqj7jFzsZM&ab_channel=GreenPeople>
* <http://websafe2k16.com/>
* <http://myfavoritecolor.dorian.systems/>

## Printing colors

<https://www.guide-gestion-des-couleurs.com/introduction-gestion-des-couleurs.html>
