title: emoji as black and white symbols in the browser
tags: emoji, typography, font
status: draft

# Using black and white symbols and emoji fonts in the browser

All started while doing a webdesign work in art school in which i wanted to have basic black and white pictogram. I naturally took the direction of unicode symbols, because a bit familiar with unicode blocks, and above all loved the idea that those pictograms or symbols where not encoded as images but really as text, a standard (maybe universal) writing that convey meaning.

## Before emoji

### Miscellaneous Symbols Block

La solution à l'ancienne (bien avant les emojis) c'est les caractères du block unicode Miscellaneous Symbols (genre le parapluie, bonhomme de neige, symbole astrologique, etc), la plupart des fonts systemes les ont mais c'était très limité en terme de choix.

### Webdings

Ensuite t'as les fonts comme webdings, qui possède plein d'icones à la place des glyphs unicodes. Le problème c'est que c'est pas les "vrais" glyph, genre la lettre 'a' ça va être un vélo, etc. Du coup tu es limité a ce que la font te propose comme choix de symbole.

## Emoji

https://www.youtube.com/watch?v=5OPkGQoPeHk

Puis les émojis. La plupart des émojis sont dans un block unicode emoji spécialement aloué pour ça, mais certain existait déjà dans le block Miscellaneous Symbols, du coup les emojis c'est juste des caractères comme les autres éparpillés dans certains blocks spécifiques.

En fait un emoji est la même chose qu'un glyph unicode.
C'est juste que les font emoji vont render ces glyph la sous la forme d'emoji.

Notons que seuls certain glyph unicode ont un emoji associé, et que dans l'autre sens il existe des emojis dont le glyph unicode n'as pas de render en texte dans la plupart des font, c'est-le-cas notament pour les 'nouveau emoji' qu'on a rajouté en les associant a un index unicode qui était vide jusqu'alors.

### Fallbacks

La plupart des fonts ne vont pas inclure les emojis.
Pourtant dans beaucoup de logiciel ou de navigateur web, il va quand meme t'afficher l'emoji, même si la font que t'utilises ne l'as pas, ce qui se passe c'est que il détecte que c'est un emoji et decide le render avec la font emoji lié au logiciel ou navigateur web dans lequel t'es.
Les font qui renders les emoji sont peux nombreuse et sont presque toujours en version couleurs.

screenshot d'inspection navigateur d'une font ou juste l'emoji est computed dans une autre

HOW Fallbacks works for browser and os!!
Depend on browser
Depend on OS (and mobile / desktop)

ya genre 5 fonts emojis mainstream pour le moment créé par les meilleurs lol: apple, google, microsoft, facebook, twitter).

Donc si on utilises un emoji qq part il sera 99% du temps en version couleurs selon une de ces 5 fonts.

## Forcing emoji to be black and white

Let's say our goals is to make a website with emoji that is going to render as bw text for every users.

### VARIATION SELECTOR-15

https://mts.io/2015/04/21/unicode-symbol-render-text-emoji/

Il existe un caractère unicode invisible qui mis à la suite force les emojis à ce render sous forme de symbole texte (sans couleurs et simplifié) plutôt que dans son design classique. Si tu travailles en html/css tu peux l'utiliser facilement.

Sauf que pour que ça fonctionne il faut que la font que t'utilises (ou celle automatiquement remplacée - qui parfois dépend du navigateur ou de l'OS de l'user) possède une version "texte" de l'emoji que tu veux render, et c'est pas toujours le cas.

Par exemple sur Mac OS et IOS le fallback remplace par une font emoji qui a que des version couleurs apple emoji, donc le variation selector sert a rien.

https://stackoverflow.com/questions/32915485/how-to-prevent-unicode-characters-from-rendering-as-emoji-in-html-from-javascrip

A la mise en place des emojis a été créé un variation selector (un charactère unicode vide qui impacte le render du charactère précédent) qui permet de dédoubler chaque glyph en trois versions: default, texte ou emoji.
Le variation selector VS15 (&#xFE0E; en html et U+FE0E en utf-8) force l'affichage texte.
Le variation selector VS16 (&#xFE0F; en html et U+FE0F en utf-8) force l'affichage emoji.

https://stackoverflow.com/questions/32413731/color-for-unicode-emoji/40020609#40020609

### Picking a font that has black and white emoji

Du coup une autre solution plus radicale, c'est d'avoir une font ou tout les émojis sont par défaut render comme des symboles de texte.
Il sera toujours en version texte, meme sur un site web, peu importe le navigateur ou l'OS de l'user.

1. tu évites le remplacement automatiques vers une font emoji système car celle que tu utilises à déjà un render pour l'emoji)
2. la font choisis affiche les emoji en bw

Impacte de cette solution:
* si on importe une font avec bcp de glyph (genre 1000, plutot que les 256 anscii), ça prend beaucoup plus de temps à charger la page.
* par contre on a un controle total sur l'affichage du glyph qui sera tjrs comme il est dans la font.


## Symbols

Note that symbols and unicode are fundamentaly the same.

Let's say our goals is to make a website with symbols like arrows, stars, complex unidode, etc and want to be sure thay are going to render for every user (no default unicode box).

### HTML entity

Prendre des glyphs uniquement dans cette liste:
https://www.w3schools.com/charsets/ref_html_symbols.asp
(dans HTML symbol)

Des symboles supporté en HTML 5.
C'est-à-dire que les glyphs présent dans cette liste sont normalement garantis que les navigateurs possèdent une font de substitution qui peut les render.
* on ne doit pas choisir de font explicitement, c'est-à-dire pas de fichier lourd supplémentaire au chargement de la page.
* par contre on a pas de controle sur l'affichage des glyphs qui variera en fonction des fonts de substitutions disponible sur la platforme qu'on utilise.

# FONT RESSOURCES

add creation date - last updated date.

the unicode block it entierly cover

Tool to see which font has what
https://earthlingsoft.net/UnicodeChecker/

## Segoe UI symbols

**link:** https://docs.microsoft.com/en-us/typography/font-list/segoe-ui-symbol
**summary:** *The Segoe UI Symbol font from Microsoft contains an extensive range of symbols, emoji, pictures, dingbats, icons and images.*
**made by:** Microsoft Corporation.
**cover:** All Emoji and all Symbols.
**license:** © 2016 Microsoft Corporation. All Rights Reserved.

https://en.wikipedia.org/wiki/Segoe#Licensing_controversy

## DejaVuSans

**link:** https://dejavu-fonts.github.io/
**summary:** *The DejaVu fonts are a font family based on the Vera Fonts. Its purpose is to provide a wider range of characters while maintaining the original look and feel through the process of collaborative development (see authors), under a Free license.*
**cover:** lots of Symbols.
**license:** https://dejavu-fonts.github.io/License.html

## Symbola

**link:** https://dn-works.com/ufas/
**summary:** *Multilingual support and Symbol blocks of The Unicode Standard*
**made by:** George Douros
**cover:** emoji and symbols !
**license:** General UFAS Licence
Free use of UFAS is strictly limited to personal use.
All fonts and documents are Version 13.00, released March 25, 2020
For additional information email: ufas@dn-works.com

Seems it was used in a archlinux package, at a time where there was no licence?
https://aur.archlinux.org/packages/ttf-symbola/

##VG5000

some but not a lots
velvetyne

## NotoEmoji-Regular, OpenSansEmoji, Android Emoji

Those fonts share the same style emoji.

### NotoEmoji-Regular

**link:** https://github.com/googlefonts/noto-emoji
**summary:** *Color and Black-and-White Noto emoji fonts, and tools for working with them.*
**made by:** Google Inc. (Antonio Rojas)
**cover:** All Emoji
**license:** SIL OPEN FONT LICENSE Version 1.1

### OpenSansEmoji

**link:** https://github.com/MorbZ/OpenSansEmoji
**made by:** Google Inc.
**license:** Apache License, Version 2.0

Merged version of:
* OpenSans.ttf (Regular) for letters, numbers and punctuation
* AndroidEmoji.ttf for Emojis < iOS 6
* Symbola.ttf for Emojis >= iOS 6

### Android Emoji

**link:** https://github.com/delight-im/Emoji/tree/master/Android
**license:** Apache License, Version 2.0

## OpenMoji-Black

**link:** https://openmoji.org/
**summary:** *Open-source emojis for designers, developers and everyone else!*
**made by:** HfG Schwäbisch Gmünd by Benedikt Groß, Daniel Utz, 70+ students and external contributors.
**cover:** emoji
**license:** CC BY-SA 4.0.

## GNU Freefont

**link:** http://savannah.gnu.org/projects/freefont/
**summary:** *We aim to provide a useful set of free outline (i.e. OpenType) fonts covering as much as possible of the Unicode character set. The set consists of three typefaces: one monospaced and two proportional (one with uniform and one with modulated stroke).*
**made by:** GNU Project
**cover:** some symbols and emoji but not a lot
**licence:** GNU General Public License v3 ou ultérieure

## Emoji Symbols

**link:** https://emojisymbols.com/beforeuse.php
**summary:** *This is a Web font specialized for use on the Internet.*
**made by:** ???
**cover:** emoji.
**license:** Copyright statement is obligatory Necessary
(To be stated as a comment in CSS). Use other than the use on the Web pages and Web services (commercial printed matters, magazines, images, and software/hardware installation) Additional support for a fee.

## EmojiOne/Two/Joypixels

https://github.com/EmojiTwo/emojitwo
**license:** CC-BY 4.0

There is an old version of emojiOne which has bw emoji but not very cool.

"The web's first complete open source emoji set."
EmojiOne has become JoyPixels (not free anymore), and also forked to become EmojiTwo which will stay free but not sure if it will be updated.
Though emojione/two are not font file but collection of .png and .svg used with a CSS API.
The first version was created by adobe?

## Refs

* https://graphicdesign.stackexchange.com/questions/113388/is-there-a-free-open-source-black-and-white-emoji-font

* https://wiki.archlinux.org/index.php/fonts#Emoji_and_symbols
