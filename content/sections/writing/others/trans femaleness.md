title: Trans femaleness meanders in between words
tags: gender, language, bodies
status: draft

[TOC]

<!-- # transness energy lies in the practice of language, our bodies are words and oppressions is implemented in their meaning and usage like abrasive chemicals  -->

## Intro: Materialities above social niceties

First i would like to situate my experience.
I am a 29 years old trans woman, i've been out since i'm 26, i'm white and - i come from a very recognasible through my ways of writting - academic and artistic background. I consider my transness to be something that acted as a compagnon, sometimes dark sometimes bright, present for my whole life even before i social constructed my notion of gender in my child years and that was there even before i started requestionning the gendered notions that where applied to my personna during my teenage years.
Because I think trans theories and theories of transness is not something that eleborate in just some big piece of media like books or movies, but through a multiplicity of voices like an ambient polyphony that requiere attention to capture, i constructed this text using the voices of others, as fragment that echoed with my personnal experience of transness. This process is also an invitation to the cis reader to develop this attention and curiosity to trans dialogues. I am reconnaissant from the bottom of my heart to every woman i am using their voice through this text.

> Liberals often parrot the line, "you don't have to understand something to respect it". And that's true! But ideally, people would understand why trans women are women, and integrate us into their idea of femaleness as equal players. Right now, I think **accepting trans woman as women is more of a social nicety**.
>
> -- Lily Alexandre, "Girls Own the Void: Trans Women, Alienation, and Metastability", [youtube video](https://www.youtube.com/watch?v=5Abzhrt-Z6g), february 2021

I agree with Lily, and sometimes this feeling of social nicety can feel heavy. Asking to be recognised as who we are, and have some kind of missinformed promesses that you'll not hurt us is a good start yes, but by cycles, alienating feelings and materialities comes back. No gender is not over and being kind is not enough. 
But so what am i asking for, and what is there more to understand than respect? Well let's try to open some boxes hidden in the corners of transfemininity, boxes that do in fact takes some space and can make us trebucher or bend or unable to sit confortably.

Also sorry already to formulate the section headers as midwest emo song title.

## The intrisinqly ambiguity of trans femaleness

### Crawling semantics of us

As a starting point, i would like to address this spiky point where everything balance itself: the cis trans difference. I collected three voice on it that i find relevant to conflict.

![](/images/articles/trans/auto_ano_beingwoman.png)

> [...] I think what I am talking about here might be the difference between transwoman and trans woman. You see we trans women insist on the latter spelling because we insist that trans is an adjective, a descriptor, of the type of women we are. Whereas transwoman without the space, suggests not a type of woman but something related to but not the same as woman... a compound word/noun in which woman is only part of it.
> **I think a lot of us are afraid, regardless of how they might spell it, that when someone sees us the thought/concept they have of us is transwoman not trans woman.** Which is probably true but also very harsh, because it raises a serious question of if that person would ever just think/say woman about us.
> 
> [...] I think it might also be true of other types of women. I susspect, based on having lived through the Bush presidency, that a lot of islamophobes think Muslimwoman nt Muslim woman when they see a woman in a niqaab, as if all that fabric removes for the racist viewer the woman's ability to be taken as a woman, in a similar way that my history removes it in some people's eyes from me.
> 
> [...] You see I don't really have anything against adjectives. In fact some people close to me will tell you "good girl" is an adjective-noun combinaition i quite enjoy. That is of course because good here is an additive. It adds something onto the girl. Likewise "bad girl" is of course a defamation, but it's not actually a detractor. it still adds something on top of the word girl. Transwoman, trans-woman or trans woman (but thought transwoman) can be a detractor. It doesn't add anything onto the woman, though we really hope it does. Instead it chops off a little piece of the woman. It says at best "she's a woman, but a little less", or "she's a woman with a little chopped off" or worse "she's like a woman". At the heart of a lot of trans politics and performance is the desire to remove this.
> 
> [...] This is of coruse what a lot of trans women mean when they say they want to be "accepted". Unfortunately when we say that, what a lot of cis people ear is that we don't want to experience prejudice and want to be treated nice and politely, when in actually what we mean is "I want to treated in my gender like you are treated in yours". And it's why questions like "why did you become a women" are not only rude but simply misplaced. They mistake transition of choice. They mistake gender for lifestyle. They mistake history for ontology.
> 
> -- autogyniphiles_anonymous, "On Being Taken As a Woman", [instagram post](https://www.instagram.com/p/CW7TTQZMr-t), december 2021

To me this same kind of distinction is what makes me bouuuh when i ear 'identify as' instead of just 'is'. it feel like we are weirdly trying to create an equivalent of woman that could be proven to be "identical" but which is still different from simply being a woman. the more i ear "i identify as" the more i internalize "i have not the right to simply say i am".

So this is for the semantics let's say, how to use words and by doing so use their meaning. And this is fundamental to not make this categorisation mistake in our own mind. Though we're still at the surface, this mistake sadly constitute a well-known-and-experienced example of the difference between respect out of social niceties and understanding/empathy on a deeper level. However let's put it in perspective with another tonality.

### Balancing differences and connections

![](/images/articles/trans/lily_void.png)

> [she's speaking about how trans women have to go through unstable life, which even if stable at some point is always subject to crumble because of their transness] This gendered experience of metastability, to me, is significant enough that I don't think of trans women as inhabiting the same femaleness as cis women. I mean, we may feel the same as cis women, but gender isn't just feeling, it's a dialogue with the world around you. You express your gender to the world, and the world answer in turn. The feedback we receive as trans women is different - people expect different behaviours of us, and punish us more severely for stepping out of line.
>
> -- Lily Alexandre, "Girls Own the Void: Trans Women, Alienation, and Metastability", [youtube video](https://www.youtube.com/watch?v=5Abzhrt-Z6g), february 2021

From what Lily says we can feel that however both cis and trans are woman there is a radicaly different lived femalness between the two. And I also do agree with this semantic. The paradoxicallity of transfeminism start to surface, how can we adress transfemininity in all it's complexities and specificies *while at the same time* stay strong on the front line of making people understand that we are just women, in the most broad and simple sense. This is not an unknown question. To me a lot of dialogue on transness revolve around this at some point, like a magnet attracting conversations to fall yet again into those eternal questions. The following tweet from judgementshae and its response echoes that quite clearly.

![](/images/articles/trans/shae_notunderstanding.JPG)

> are you a "the differences between me and cis people are irrelevant " trans person or a "cis people are fundamentally incapable of understanding me" trans person?
>
> -- judgementshae, [twitter post](https://twitter.com/judgementshae/status/1701775021088051345), september 2023

Not surprisingly, lots of answer of the post directly empathize with the question of understanding, trying to create bridges in between cis and trans femaleness is depicted as essential though every trans woman where able to sense the materiality of this gap in their lived experience - while cis people may simply not see it, in a naive but potentially hurtfull manner.

![](/images/articles/trans/shae_answer1.png)

![](/images/articles/trans/shae_answer2.png)

![](/images/articles/trans/shae_answer3.png)

![](/images/articles/trans/shae_answer4.png)

Ok so, "we're fundamentaly different while being just like you" says the trans women.
To not loose ourselves into abstractiveness it's good to remind that **both of those questions are materialistic needs for us: meaning they shape how we construct ourselves as individual through lived experience, but also by rejection or appropriation they can put us in danger from ourself and others**.

* Forgetting our differences can be lifely dangerous for ourselves because it means forgetting who we are and how we constructed ourself only to "fit in" to other people criteria.
* As much as feeling excluded from femaleness because of pointing those differences and constructing the deadly ambient idea that we're a third kind separate from woman.

However, this paradoxicallity can reminds us of other feminist stories.
While there are multiple form of woman, and equivalent intersectionnal feminism have been trying to include their experiences as the one of woman, there is a specific struggle on this example of trans women.
We agree that we need to address the multiplicity of woman experiences by using precise terminology, giving context, including social classes, religions, culture, bodies, etc; like it's important to talk about working/precarious woman, women who are mothers, woman of color, woman outside of gendered or bodies normativities, etc; but
**for trans woman the sole construction of such a theory can be at risk**.
An enormous part of transphobia is due to not being considered as a woman in the same sense than cis woman are. 
If our response is to create a discourse that continually quote trans woman to speak about our own life as a separate experience than the one of cis woman, it becomes materialisticaly dangerous and can be (even inconsciously) be weaponize against us by creating the ambient though of "you see, if you have to come up with a whole theory on your of woman as something different than cis feminism, then maybe it just makes sense you're not part of it". **While whole reasonning doesn't have the purpose to emcompass everybody experience of trans femalness, it does constitutes the intrisinqly ambiguity of it**.

Our femaleness is intrinsinqly ambiguous, as unfolding its specificities marginalize us as much as it is a need.
**The consequence of this ambiguity aren't only societal or politic, I realized not so long ago that my construction of certain pattern of attachement, fear, hope and desire might be linked in some way to this balancing point, and that by making it explicit it could start to distress some personnal mind knots**. Why do I want cis woman validation as part of their group as much as I want them to recognize I've had to contruct myself differently from them and this resulted in being *not the same*.
Lily alexandre, in her video essay, very couragouly address this trans and cis woman differences, it was for me a revelation to hear it. 

> quote on lily speaking about transfem masculinity
>
> -- Lily Alexandre, "Girls Own the Void: Trans Women, Alienation, and Metastability", [youtube video](https://www.youtube.com/watch?v=5Abzhrt-Z6g), february 2021

## Emptiness navigators

### Gendered role and the absence of it

colonial definition of woman

like they are "bad woman role" considered as opprevise
but they are no "good or bad transgender roles", as society just didn't take us in account at all.

to quote lily alexandre on "transgender roles".

the reflection society gives us is not often enough a bad one,
like cis woman can be seen as caretaker, nursing, or wives, this is still a role
quote lily alexandre on "what is woman" and colonial definition of womanhood
society, at no point in time, had a need to give any gendered role to trans woman.
while cis-feminism state that not all woman should be mother, trans woman don't have the right to be a mother, neither they have the right to be something else in fact.
while "woman liberation", as a way to escape from an oppressive gender role, make sense for cis woman, trans woman have no oppressive gender role to escape from, as society never considered us enough to acknowledge that we could take any role.

this is why trans woman often lives in interstice of society itself: 
- sex works
- coding
- dull simple job where we either stealth or boymod

in all those three we have to learn to perfect the arts of discretions.
creating precisely crafted online presence, acting behind a computer , being able to move quickly in public spaces without taking care of glances.

this is why lily scoffs when she heard that "congrats for living your best self!! you're so strong!!"

### Trans visibility

quoting mimi zima on trans day of visibility

this is also why trans representation is still so weird.
there is this immense gap between us being killed and being an icon, fetishised pop-star, for the exotism and voyeurism of cisness. i can't be proud of 3 instances of transfem bodies being mainstreamly adored (and sexualized), our struggle romanticized. i wouldn't never say to a cis woman, pointing out a parfum advert "look, society recognize your existence, and even more: they want to fuck you".
when we're speaking about trans representation this is what we're often speaking about, that a transfem person is litteraly just there and that people kind of like her and the cis are all smiling like "heeee trans woman is there!!!".
even in representation we are an image, however a glorified one, but still one which is carefully painted out of void, to be sure that this is still one step away of any conversation about our gendered role and existence in society.

post mimi.zima on trans day of visibility

so no liberal politeness is not enough for me.

### "Trans woman are woman"

If we're going to address those semantic question, then what about this slogan rigth?
For that allow me to invoque mother contrapoint

> "trans woman are woman" centering on the definition of woman, which is as lily puts it defining women was always containing an history of oppression intent
> "trans liberation now" imply political revendication

saying trans woman are woman could even be seen as, outside of the obvious clarification of semantic ambiguity explain in the previous section, imply that we are reclaiming normative woman gender role, like to be mother, house wife, etc. Which ironically, because it's still hard for us to access those, there exist an intersection of really conservative looking meme produce by trans woman as a way to both reclaim those while being cinical about it. (tradwive posting, breedable, etc)

although the me, the term liberation is still to anchored in this idea that we are being captive in certain role, while our main role is being the interstive void explorator.
i could propose a new one that would be something like

"trans power now"

because well, yess i am asking for something else that basic right of existence. 
woman have been asking to get important role in society, to get power, and it feels ok for them to ask for it , it's subsversove, while trans people asking for power its out of place, really ? such a msasculine notion as power? it though you where all about being soft and elegant butterfly angel of something? No actually we want to have the power to hurt the one we don't agree with, exactly like cis man (and sometimes woman) have been doing for centuries and we never have.
I mean if you never feel threatened by us then it's not revolutionnary, it's just reappropriation.
i am simply asking the same, playing on the choc it can trigger than suddenly we are asking for something more than survival or basic help, and if deep inside, you feel threatened by trans people asking for power.

## The forbidden reflexivity of the question of transness

### Bodies and words

transness and languages intertwine so much.

i think when i started thinking of myself as trans, everything was all about bodies, and oh my bodies lead to different type of conversations than words. 
speaking about bodies you speak about gender expression, transitioning, public harrasment, hormones, the fear of going out, feminity, masculinity, muscles, boobs, the shell that you inhabit, the connection between your mind, taking up spaces, moving through spaces, growing, changing, etc. when we speak about body we directly reflect on our own, the conversation feels emboddied by our own presences, our own thoughts about ourselves. we ask ourselves the questions of the conversation as our own body was the subject, discourse on body act like direct mirrors.
speaking about words you speak about academic studies, languages intellectualisations, poetry, very white and privileged things that seems totally disembodied and for the pleasure of intellectuality and reasoning. 

bodies <--> words

the sole act of writting about trans woman can be used has some proof that we are fundamentally different from cis woman, and from that helping **to construct the lattent ambient** idea that 
"even if trans woman need basic respect of pronouns out of politeness, 
we deep down kind of know that we don't *actually* need to try to understand and acknowledge their form of existence as something that construct society as much as we, the cis."
an idea that slowly kills lives.
not as an direction violent aggressions, words can't make you bleed, but usage of language can change people, and then society can prescribe certain change, to maintain idea and the ideas being there ambiently in their very own language like viruses, can slowly encapsulate minds in certain direction, and they can at a certain moment turn into gestures, gestures that can litteraly: kill.

but bodies are as political as words, and sometimes in the very same sense.

### Our existences as cursed mirrors

an example of the later is the violent transphobic reaction it can create in someone else when displaying transness.
the things that make this example interesting to me is that there it doesn't matter if its bodies or words, in the eyes of the scared transphobic it reflect the same on their own body: violently.

so here's how it goes.

the existence of homosexuality have a reflexivity on straight person. be there oppressive or not, they know that gays exist, and their existence culturally and invidually implemented straight responses.
culturally it can be like "hey i'm not gay" (the famous "no homo bro") or else "go suck my dick" (implying gays are inferior).
individually it can be like as simple as a "do i have desire for same sex person?" that one would ask themselves as a teen or young adults these day, and even with some internalized homophobia you can still be able to geniualy ask yourself about your personnal preference. either saying "no i'm not!" violently in defense of that thought, or "interesting question but no" with more honesty.

the thing is that for transness the reflexivity it has on cis person is totally far behond a simple symmetricality of reasonning. the mere idea of transness outbound social normal and people constructed universe so much, that the genuin question is out of reach, even for other "queers" or whatever kind of open-minded-bienveillant people.
the reflexivity can not be one of individual identification and rejection process, it can not be one of empathy with the question "am i trans", neither it can be one of violent rejection of the question "am i trans".
while homophobic people reject the question, like "i'm not gay!!!!!" in diagonal violant way: expressing toxic masculanity, use gay as an insult, or even as it's most the case these day 'ironically' rejecting gayness, etc.
transphobic are never going to say "i'm not trans!!!!!" because for that to happen they should first aknowledge that our existence imply that every human being is a valid subject of the question "is this person trans".
and their world is so not ready for that kind of disruption of gendered social order.

hence the reflexivity that comes out of displying transness, *either by use of word such as gendered pronouns, the word 'trans' itself, or the looks and expression of trans bodies*, is one of complete negation (not always a violent one).
it can either be to state that we should not exist in direct harmfull ways such as physical violence, legal violence, etc.
or it can be this simple ambient politeness, the social nicety recommanded to not be considered as a bad person, but without any real form of reflexivity.

### Gender reflexivity exercice

i want you cis readers to take a second, even if you already did it once that kind of experience, to ask yourself those question. if you're reading this text chances are your the kind of cis person to already ask you that kind of question, but i would like you to do it one more time, sincerely and naively.

- am i trans?

then comes the real forbidden reflexivity

- what did i use to answer the first question: language? expression? memories? feeling? did i tried to proof stuffs? or to define stuffs? how can i precise the process i went through by asking myself that?
- did i had feeling of certainty or ambiguity? what felt ambiguous or uncertain? what felt clear and certain?
- do i think others would go through the same type of reasonning as the one i did if they were to ask themselve the same question? try finding people who you think would use the same argument than you, and one where you'd feel they'd use totally different one
- start the sentence i am ... because ... try variations of that sentence and say them out loud
- did i felt resorting to gendered clichés while completing the previous sentences?
- do i think other gender could share certain of the things i associate with my gendered existence. typically if i resorting to calling me a woman because i am better at care then men, do i think men could be better at care than me.
- could i still be me, if i were of a different gender? i would i then live, what would it change.
- is there people in my surrounding i see as gendered figure, such like a mom, a close friend of same gender than me, a close friend of a different gender than me. list at least 4 people like that, one you feel their gender.
- ask yourself, what makes me receive their gender? is it simply the pronouns they use? is it their attitude toward certain things

of course it's worth nothing that those question only work if you allow the reflexivity, if you allow yourself to say "i am trans" or "i am not trans". if you are answering the first one with the state of mind of either: trans person does not exist 'biological chromosomes and whatever' ("the bad guy of the episode kind of answer") or it is simply impossible for me to be trans period ("the real evil of the show"), then you're not playing the game:

you have not unlocked trans reflexivity yet and this zone of the world is forbidden to you

### Angry about pronouns 

if you played the last exercice correctly it should become clear that pronouns are of the most emboddied words.
they can by themselves redefine someone in your head, trigger violent reaction.

gender is not about writing manifest and deconstructing stuffs through intellectual and aesthetical practices as a playground,
it's about people and lived realities.
and it's making me particularly angry.

if gender theories, or (to be more direct) the existence of trans people,
is for you some kind of theorical background or material that you can feed off for your own little reflexions on gender.
if (your) queerness is something that is about intellectualising your point of view on society, rather than a lived day-to-day reality.

if you speak or work around queerness in any kind of way, but feel not consistent in being able to respect trans people pronoms and identity, then don't.

i certainly don't want to hear about a "language mistake", there are no such thing.
if (your) language is directed by rules that you have to "respect to be a good person", rather than by the believe it makes a loop coming directly from your own lived experiences to circle back to change your own lived experiences, continualy articulating and updating perceptions.

ask yourself: how does it comes i don't misgender cis people, and that when i think about 'dad' or 'mom' i don't swap them in my head, but when it comes to trans people i may misgender them (even though i'm reminded constantly of their pronoms).
as long as your own prism of perception is implicated, there is no such thing as a purely "language mistake" like if it was disconnected from your personnal relation to gender.
if you're doing "language mistakes" for trans people (but not for cis people) then *you* are simply not sincere about your own relation to gender expression and reception.

you misgendering me only says things about you not about me, i know who i am.
it says how you are percieving me, and to come to realise that people who have been revendicating a certain queerness and that i'm used to know are not even percieving me as i am is hurtful, because it's both not respectul to me and unsincere toward yourself.
Please deal with your contradictions.

meme about pronouns

## Cis people made gender, it's not about us

this concept that cis people invented transness, in the idea that i'm trans in the first place because of the cis normativity. i had to come out because cis is the norm and that made me trans. discussing about "trans people" is firstly discussing cis-normativity and all the gendered system created by the cis for the cis. i want a shift in discourse, i want the conversation about gender to not be about trans people, as if we are the marginals for escaping the little rules, i want the conversation about gender to be about cis people. cis people invented gender, cis people invented normativity, cis people invented gendered role, cis people are the one pursuing this to exist and enforcing it into people. 

i want that when talking about transidentities, we shift the discussion about cis people as much as possible. 
example: 
* instead of discussion trans gender presentation, let's discuss how cis poeople percieve gender presentation in their everyday lifes, what's is profondly so weird about their gender perception and how they act accordingly, let's also discuss how their own gender presentation matter to them anyway (may it be conformative or the opposite!)
* instead of discussing how misgendering affect trans people (that is in no way understandable to any cis person imo), let's discuss what does it say about the cis that they still misgender me after knowing myself and my pronouns, like what does it says about their own view of gender and language, or about their own capacity of real empathy through shift of perspective, let's discuss about how much their mind are heavy like stones and moves at the speed of slime on the subject of gendered language. how they are tricked by their own language biases they have created for themselves even "when doing an effort". let's simply discuss their own difficulty about gendering us, as we would discuss a little child difficulty to articulate something simple.  
* instead of discussing hormone access and care for trans people, let's discuss how the cis have access to their own needed medication, whit what facilities, which kind of psychological test do they have to deal with in order to have contraceptive medication or anything? 
* instead of seeing transness as trendy and discussing what how trans is so cool and radical, let's discuss about why the cis are so weirdly attracted to transness, let's discuss the cis-fetish and admiration and appropriation of transness    
its often caricature that trans people are like gender expert, that they overcame gender in same way
and that it's asked from us to know gender very well in order to defend ourselves in many situations

well, my brother in christ, you made gender, and imposed it, enforced it, sociatalized it, normalised it, turned it into a banalisation, a rule of existence deeper than both bodies and words. still you as the cis-person living deep inside this very constructed of a world, you know nothing about gender.

i'd like to switch the table and attack the cis ego a little bit.

i think more conversation on gender should center around cis people and not trans people.

for example something out of the order of casualty for trans person is someone defending an informed behaviour by stating that "she does not know a lot about transidentities, this is new for her, but don't worry she's going to learn and try her best, she just need some time to adapt".

she is un-informed, and ignorance can hurt

well yess but i don't think this person should inform herself on trans identities, as if: we were a new species that needed a specific investigation to be understood. as if we were alien out of the margins, again derivating far away in the void, and that ones need to use a telescope to study us from the normal center of the world to be then able to respect us.

the telescope approach is always going to put us appart, as emphazised by the paradocicallity of transfeminism.

well it's not a telescope you need, it's a mirror.

i think she sould not study "the case of trans people" but do something maybe a little bit harder, which is study "the case of cis people", and slowly understand that she is as gendered as us, she use pronouns as us, she has gender expression just like us, she is sometimes incertain about her gendered feelings, and sometimes like to push on the boundaries of gender herself.

i want her to become conscious about both bodies and words as powerfull objects.

i want her to think about why she can feel attracted by us, why she can feel threaten by us, it want her to genualy process a reflexivity of transness, in all her cisness playing with her own perception and experience of gender in her hands. and then when she feel she grasps it more she can starts asking other questions about their's.
