title: about lists
date: 2023/12/20
summary: list can be multiple things
tags: list, poetry, technology
status: draft

[TOC]

## simple

i love how a list can be at the same time

* a poem
* a technology

i get high on simple ideas, the simpler the idea the stronger it hits me.

<!-- a friend told me _simple_ is "not doing more than what is right". i am still not sure i think this is what simple mean but i think this definition is simple. -->

i have been wondering what was the first list made by humans.
it mades me think about the first calendar of 28 lines carved on a bone made by a women, that could be about menstrual cycles or moon phases. or said simplier: listing transformation through time.

![](/images/articles/lists/bone.webp)

i think lists are a simple technology.

i remember the artist SOPHIE saying that her music has to be simple for it be loud. as if simplicity was a way of being able to make everything louder. and that is what pop is all about.

i think lists are a loud technology. lists are a pop technology.

## technology

lists are a great technology because they're like counting but with actual things instead of just numbers.

![](/images/articles/lists/montesorri.jpeg)

here are some of my favortie lists as human technologies

* a recipe, a list of steps
* a calendar, a list of moments
* a library, a list of categories
* a dictionnary, a list of words
* a choregraphy, a list of moves
* a melody, a list of notes

![](/images/articles/lists/gps2.JPG)

i think a TODO list is a bit of everything and its why they can be hard to actually do sometimes.


![](/images/articles/lists/color_blocks.png)

list can be shy too, hidding in things we think we know.
is a text nothing more than a list of sentences?
is any encoded text nothing more than a list of bytes?

something silly i like to think about is how computer graphics are lists

* image file are lists of colors
* vector graphics files are lists of moves

but if i look at pixels i obviously don't see a list, i see an image.
is being a list more a perception that a nature?

## poem

listing is a process that lie somewhere between naming and counting.

it is also re-assuring to me as it offers you one of the simpliest process.
it is simple because it has a clear start, and a precise next step.

1. name a thing
2. repeat step 1

things that you may ask yourself and that can seems like a complex question, like "how did i felt today?" can be gently tamed by the act of listing.
let's do an exercice: list the emotions you felt today.

a list of song can be a playlist you made. listening to a playlist can contains drama and story telling.
when dancing to a DJ set you ask yourself: what is the next thing in the list going to be; it is a work of expectation and surprise (note that lists can also be deceiving).
the simple process shown before, when it has already been done by someone else, and you're going through the list, is an experience of wonder.

1. experience something
2. repeat step 1

both the process of listing and experiencing a list unfold in time and are done by people.
they are hidden performances, like reciting, we practices more often than we think: both during a one minute conversation and year long existential cycles.

![](/images/articles/lists/playlist.png)

the poetical nature of a list come from its rythmic element.
because a list has a clear unit of fragmentation: the things it is listing.

![](/images/articles/lists/poem.png)

here is a nice poem it goes like

1. monday
2. tuesday
3. wednesday
4. thursday
5. friday
6. saterday
7. sunday

<!-- ![](/images/articles/lists/days.png) -->

i have repeated this poem so many times in my life and i still discover new meaning in it.
new rythmic variations in what i think has become the everyday, and the unexpectedness of every day on its own.

## rythm

often a list that is also a poem are ordered list.
it is because we need to go though it in a certain direction in time and that is easier to do if there is a order.

a cool list that is also a song is the latin alphabet.

![](/images/articles/lists/alphabet_song.png)

it is ordered so we can sing the same letters in unision when learning.

i learned that the symbol _&_ is named _andpersend_ because it was once the last letter of the latin alphabet and the song went like "..., x, y, z and per say 'and'".

the rythmic aspect of a list can be feel visualy in digital conversation. i use list segmentation as a way to communicate a lot though my own conversations. i like online conversation because you can play with the segmentation of speech visualy.

![](/images/articles/lists/chat.jpg)

it is important to understand that rythm is something hard to understand.
it is in a list but it is none of its items, it comes from the arrangement of elements.
this is why we need rules to create rythms, because we can not simply replace one element, changing rythm is a whole reorganisation that sometimes means to break back the list into a simple set of elements and reconstruct it again.

in music, architecture and graphic design we often say that the solution to rythm is to use grid; but what do we mean by solving rythm?

if two we recite two lists with a different number of elements this create [polyrythms](https://en.wikipedia.org/wiki/Polyrhythm). music as trouble to talk about polyrythms because its not really on one grid, and the grid itself become an opinion of the viewer.

## order

HTML is the language used to create webpages by describing them. I used this language a lot, i tink it influenced my idea of what is a list. in HTML there are two kind of list `<ol>` for ordered list and `<ul>` for unordered list.

there is a thing called semanticallity in HTML. this refer to the writing of HTML not for how it look but to say what is what. for example if you make a _navigation menu_ containing links like _homepage_, _about_ and _articles_, then your menu is a list. you should say this is a list, not to have the bullet style presentation, but to dialogue with technology and user and tell them: here is a list, even if it don't look like it.

i think more often about list as the intention of a writer.
sometimes to write something as a list of not is an intention.

unordered list are often a problem. it is like saying "what would you like to eat (in no particular order) pasta, curry or fries" and because time goes in one direction someone can answer "the first one".

i find it nice that we have a word to precise the intention of `<ul>` even though it can never really be expressed as it.

i remember a tweet of Omar Rizwan who said it would be nice if browser showed `<ul>` everytime in a different order. so the songs change everytime. here is how i would do such a script.

```javascript
let lists = document.getElementsByTagName('ul');
for(let list in lists){

}
```

when programing i sometimes feel like its all about having the pictograms 🔁 and 🔀 in the palm of my hands.
one is about the repetition of a list, also called a cycle. the other is about exchanging thing in a list.

there is a whole programming language where everything is a list, it is called Lisp.

## cycles

...

## metaphores

one of the power of the list lie in the abstraction layer it asks for by naming the things. this is what makes them different from collections. 

a collection is the set of things. 

while a list is the set of references to the things.

for example the collection of the rocks would be the rock themselves, on the floor or in a box whatever.
but the list of my favorite rocks would be using their names or images of the rocks. 
i don't care that much about collections.

this allow lists to be occasion to create new meanings. every list entry is an invitation for a metaphore.

of course many artists have used lists.

... explain that better

Yoko Ono happening are very short lists of steps

![](/images/articles/lists/yoko.jpg)

Laurel Schwulst's webpage on [japanese micro-seasons](https://pentad.world/), or how she [associated every year between 2010 and 2020 to a color to speak about the internet](https://rhizome.org/editorial/2020/jan/06/laurel-schwulsts-decade-in-internet/)

some list induce poetic meaning through the power of association.

* [beaufort wind scale](https://www.spc.noaa.gov/faq/tornado/beaufort.html).
* [Noms des anniversaires de mariage](https://fr.wikipedia.org/wiki/Anniversaire_de_mariage#Noms_des_anniversaires_de_mariage), or an english version of it [Traditional anniversary gifts](https://en.wikipedia.org/wiki/Wedding_anniversary#Traditional_anniversary_gifts)
* [Alchemical processes associated to astrology symbols](https://en.wikipedia.org/wiki/Alchemical_symbol#Alchemical_processes).

i also have a crush for very contextual list, that feels like cultural samples but remixed, like this list of typographic ornament associated to country noises.

![](/images/articles/lists/ornament.jpeg)

collections are a scary thing when it comes to non-physical things.
a dictionnary is a list of words.
but a collection of all the words is all the words togethers, not on paper or on your screen.
just all the words together, from their materiality of words. 

collections are handled by mathematicians or phylosophers. they have to look proud to carry those infinite weights.

a list is light because it does not contain the listed things in themselves. so relieving.

we can list anything we want, how far, how innaccessible, how immaterial, how abstract the thing is.

![](/images/articles/lists/aztec.webp)

list can be used to apprehend heavy collections, like a calendar does with time. otherwise we would feel so lost in the spiral of time without any other marks than a vague feeling that we are not yesterday anymore.

making list is feeling meaning gliding in our hands.

## consumable

because of this, list have been use as synonism of efficiency and optimisation in work culture, that expended into culture. 

* "10 ways to improve you productivity"
* "20 things about this movie you may have missed"
* "6 dresses i wich i could buy"

the process is so conforting, and predictable, the outcome is so simple, and easy to digest. it is a good medium for consumption.

easy for capitalism to make us consome anything.
even work.

this is also why list like todo list can be dangerous, because listing become a process of fragmentation in order for productivity to take place. by going from one fragment to another, and adding new items in your todo, the process can start being the one of consuming, never looking from outside the list and only looking at the five remaining step that you urgently have to do in fact you're already late.

fragmentation also allow discussions and discussions are important, even discussions with the self.

list can display power and thus be critical on it. the list of [top-level-domains](https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains#ICANN-era_generic_top-level_domains) make me thing about how corporation can own words.
standardization is often a process of listing, but i'm not sure about that one.

* [Unicode character](https://en.wikipedia.org/wiki/List_of_Unicode_characters)
* [W3C color names](https://www.w3schools.com/colors/colors_groups.asp)
* [Pokemon types](https://bulbapedia.bulbagarden.net/wiki/Type)
  
## tree

there is a famous wikipedia article that is [a list of pages that are lists of other list articles](https://en.wikipedia.org/wiki/List_of_lists_of_lists).

it uses recursion.

recursion is when one of the thing being listed is itself a list.

![](/images/articles/lists/FS-layout.png)

recursive list are also called tree.
the list inside the list are the branches.
you may have seen trees in said "table of content", when they show a section inside a section, listing the subheading of the heading.
in most cases, trees are shown with a new level of indentation at every branche.

trees are everywhere in programming languages.
if you're used to write code, you may think of trees as abstracted complex data-structure.
but the writing of the programming language itself can form trees.
in javascript, tree can be found as functions, loops or condition inside functions, loops or conditions.
a tree in javascript writing would look like

```javascript
{
    {
        {
            {}
        }

    }
    {
        {}
        {}
    }
}
```

![](/images/articles/lists/Fractal-plant.svg)

some of my favorite examples of tree are

* the DOM of an HTML documents
* the file-system on a computer
<!-- * the way branches divide themselves in a tree, and the associated [L-system](https://en.wikipedia.org/wiki/L-system) -->

quick note that a table, or a spreadsheet, is nothing more than a tree of level two, like a flat 2-dimensionnal tree, a simple list of list.

## identity

a good question is a question that triggers listing in my opinion.
it is a trick so the answer can expand forever and to engage together in the creative process of listing, in all the poetry of collective protocol and associations exercices.

a question i love is "what is your favorite (animal, color, ...) ?".
every list means you can ask someone their favorite.
and if you ask it to a group you get a new list, the list of everybodies favorite something.

## morphism

for a long time people have asked me how my studies in mathematic helped me doing design and web-development. often their questions contains the answer they are expecting: that i can understand and code complex mechanism. but i know if something link both field it its something much more simple.

much more pop.

something about counting and naming.

in mathematic we use [morphism](https://en.wikipedia.org/wiki/Morphism). those describe a transformation from one thing to another that preserve the structure. if there are morphism in the two direction, it suggests that those two thing which appears to be different could have an underlying common base.

![](/images/articles/lists/isomorphism.png)

morphism can happen between two list. it can be flexibility of seeing list in different things and mapping them onto each others.
to translate meaning and imagine new structures.

i think designing is not so far away from morphism.

i think designing can be mostly about finding list and morphism between them. where we decide to make them and how they happen. what we decide to list.

* is there is a common structure between a recipe and a calendar?
* can a TODO list be a poem?
* if an images can be described as a succession of movements, is it a choreography?
* what does it means to give names to the days of the week?
* what are the favorites colors of your favorite friends?


<!-- * like a carved bones, one tick at a time to measure the changes in the sky. -->