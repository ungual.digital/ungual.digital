title: billingual CSS (SVG pattern and interactivy in CSS)
tags: CSS, SVG, pattern, web2print, js-less
date: 2022 12 28
status: draft

## Context

web2print a dot pattern

## HTML approaches (background gradients)

The usual know tricks

however gradient background export as image, making them pixelated and BAD

## SVG pattern

how to with SVG pattern

but sad, because with the HTML approaches, we could use _custom properties_ in the pattern so we could for example:
* have a color variable that is use both for HTML element (text color, background color, border color), and is used in the pattern
* have a size variable, same (ie making some HTML border the same width that some lines in the pattern)

Now we loosed that, because we're working with SVG.

No, we didn't, that just were the interesting part begin

## Note on billingual CSS

CSS was never meant to be only use on HTML element, it exists for SVG
expand on that.

first technical note: 
in order to use billingual

the element have to be in the document XML, which makes sences.
But also require sometimes long weird codes...

### Preparing and inserting an SVG

add on Inkscape XML editor

You can create such an SVG using Inkscape with:

* the object pannel opened, to look and interact with the nesting
* the CSS pannel opened, to clear inline styles and assign classes or id's

Still it's sometimes a bit tricky to do exactly what you want... I often close the SVG in Inkscape and open it in my IDE to do search and replace (on classes name, or inline-style for example) then open it back in Inkscape. It's a bit tedious but that's part of the craft.
With those tools we can draw the outlines layer and then the group for every studio (assigning the class: `.room`) with two child: the `.surfaces` `path`, and the `.sudioID` `text`, from which we clear the inline-style. Something that helps is to create one group with proper classes and inline-style cleaned and then copy-paste this one and edit it for every other elements with the same status.
Then we have to create the `a` element by hand, opening the SVG in our IDE and search for `class="surface"`, and manually write the `href`. Note that we could do it in python, but it was honestly faster to do it by hand for 12 elements.

Also in order to use CSS on a SVG, in a separate sheet we need
    * that the SVG is hardcoded in the html file (inserted with python or copy pasted)
    * that inline-style have been removed from element (or else it is going to have higher priority)

### Billingal words

This open to SVG / HTML manipulation in a total new way, like they're both clays of different color and you're creating a marbled sculpture, instead of simply sticking in SVG balls in the HTML structure.

We can use CSS custom-properties that are shared by HTML element, like colors!
Note that the SVG properties are not the same than the one on HTML elements.

```CSS
:root{
  --stroke: black;
  --surface: blue;
}

/* svg */
path.walls{
  fill: none;
  stroke: var(--stroke);
}
path.surface {
  fill: var(--surface);
  fill-opacity: 0.06;
}

/* html */
div.box{
  background: var(--surface);
  opacity: 0.06;
}
```

### Document relative vs Object relative

**moreover their language intersect: meaning some value can be used in both HTML and SVG. like two languages having a words in common with the same meaning (or a relatively close one).**

this is super important to keep in mind that even if you write the same properties and values couples it doesn't mean the same thing for CSS and SVG.
a super interesting example is the document relative vs object relative. 
with `fontsize: 16px;` being once document relative and on the other side svg relative.

the same apply to `stroke: 2px;` and `border: 2px solid black;` but for this one we have.

Originally, the `stroke-width` we are defining for the SVG elements are relative to the `viewbow` of the SVG. Which means that it's going to scale proportionnally with the SVG size, and that it doesn't care about if it's actually `1px` in the webpage. The unit system is intern to every SVG on your webpage, and does not synchronise by default.

We can change this using the [vector-effect](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/vector-effect) property. By combining with the previous point, we can make it matchs other element on our webpage like the borders, or even the underlines fo the links.

```CSS
:root{
  --stroke: blue;
  --stroke-width: 1.5px;
}

/* svg */
path.walls{
  vector-effect: non-scaling-stroke;
  stroke: var(--stroke);
  stroke-width: var(--stroke-width);
}

/* html */
div.box{
  border: var(--stroke-width) solid var(--stroke);
}
a{
  text-decoration-color: var(--stroke);
  text-decoration-thickness: var(--stroke-width);
}
```

note that sadly for font you'll need javascript.
here is an implementation of `vector-effect: non-scaling-stroke;` to make font size in SVG match the one of HTML elements.

## Back to pattern

Now get back to the original question: how can be have those billingal SVG pattern?

### How to apply them?

Instead of copy pasting the pattern every time in the definition of every element we could use it by _calling it_ from an SVG

### define a hidden SVG containing the patterns instanciation

first version where the pattern are defined here

```HTML
<div id="svg-patterns">

<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<svg id="patterns">
    <defs>
        <pattern id="dotted-pattern" x="0" j="0" width="5mm" height="5mm" patternUnits="userSpaceOnUse" >
            <circle/>
        </pattern>
    </defs>
</svg>

</div>
```

```CSS
#svg-patterns{
    /* we can't put it on display: none; otherwise the instanciation doesn't work */
    width: 0;
    height: 0;
}
```

Note on the fact we can't use width and height in css on pattern

Note on what `userSpaceOnUse` does.

### applying them

using `fill: url('');` and an nearly empty, placeholder SVG.

```HTML
<svg class="pattern dotted"><rect width="100%" height="100%" /></svg>
```

```CSS
svg.pattern{
    /* you have to give it some size otherwise it's 'empty' */
    width: 100%;
    height: 100%;
}
svg.dotted > rect{
    fill: url("#dotted-pattern");
}
```

the advantage of applying them through CSS again, is that then _a pattern correspon to a class on the `<svg>` element_.
meaning if we want to change the pattern we just change the class on the `<svg>`.

### declaring SVG-CSS patterns

We instanced the patterns but they're empty.
Yes, because they work with attribute, we can use CSS properties to declare those attribute.
It's like we create empty pattern as an object in the material world because they are SVG objects, they need to exist somewhere. But _how the pattern is_ we're going to say it in CSS.

``` CSS
/* DOTTED */
#dotted-round-pattern circle{
  fill: var(--clr-grid-point);

  cx: calc(5mm - calc(var(--grid-point-width) / 2));
  cy: calc(5mm - calc(var(--grid-point-width) / 2));
  r: calc(var(--grid-point-width) / 2);
}
```

the only :((( is this 5mm which is the pattern size and can't be triggered in CSS.

## another examples of interactivity

the marbling of HTML and SVG CSS languages can go further

example of the meyboom map

with the rooms being links that scroll to an id present in the text.

mermaid graph showing markdown id - correspond to SVG a anchors.

Hover effect

```CSS
.room .surface {
  fill: transparent;
}

.room:hover .surface {
  fill: blue;
}
.room:hover .studioID {
  font-style: italic;
}
```