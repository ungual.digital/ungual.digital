title: On CSS baseline and vertical rythms
tags: CSS, typography, web2print
date: 2022 09 15
summary: A crafty and hacky implementation of CSS baseline, transforming lineheight behavior, and feeling of rythms.
thumbnail: /images/articles/basealign/irma.png

[TOC]

## Craft context

I started to go deeper in CSS baseline related crafts while designing myself a personnal planner.
The planner was constructed around a 0.5cm grid with bullet-journal like dot patterns, and was entierly made with HTML and CSS.
When writting with a pen on a quadrillated paper sheet, the grid becomes a nice invitation to write your text so that its baseline matches the horizontal lines of the grid. Because the planner would contains both printed typography and my own handwriting, **I wanted to see how the baseline of HTML text could sit on the vertical grid of the planner. That curiosity became a challenge: can I do that so that it magicaly works for any font-familly and font-size by using CSS.** Implementing such a design goes against the native way text flows in a webpage and it requires to unfold some complexities of CSS typography. It is important that the context was printing, doing something this complexe for a website is a fragile path.

All those complexities that we're about to unfold come from the fact that the web revolves around different rules than desktop pubishing softwares when handling baselines. The concept of leading and line-height differs. Both tools left different type of cuts on the materials and have proven to create usable reading environment, so this is no place for typography arguments but rather for crafts.

%%%
    ![A comparison of leading from classic DTP softwares and line-height in CSS](/images/articles/basealign/leading.png){: }

    %: A comparison of leading from classic DTP softwares and line-height in CSS

## Different rythms

### The fluidity of default

Let's take at look at the _Default vertical rythm_ of HTML & CSS. In the absence of any kind of explicitly defined grid the flow is controled by the user-agent stylesheet.

%%% figure-full
    ![A two-columns example of the Default vertical rythm of HTML & CSS](/images/articles/basealign/scheme_default.png){: }

    %: A two-columns example of the _Default vertical rythm_ of HTML & CSS. 
    It is set in [Junicode](https://junicode.sourceforge.io/).
    The text and image are taken from [Wikipedia](https://en.wikipedia.org/wiki/Leaf).

In this case: line-height is unit-less (here it has been adjusted to `1.35` for the paragraph, and `1` for the headers for comparison purposes, while the default is often `1.17`) and vertical margins are defined in `em` (here they have been adjusted to `1em` or `2em` values, while the default is often `1em`).
Those defaults doesn't really care about alligning anything on a grid, as we can see by the two highlighted paragraphs.

In _Default vertical rythm_:

* the boxes of the elements constituting the text flow have totally independant heights and thus does not aligns on a common vertical grid. 
* As a consequence, the baselines of the left and right column does not align with each others.

This is easily explained because unitless `line-height` vary according to the element's `font-size` (like if it was set in `em`).
So it produces bigger line-height for a `h2` than for a `p`: every element with different `font-size` having different heights. The spaces in between the boxes are also not regulated and `font-size` dependant.

Changing the `font-size` of an element impact the rest of the document, **everything stay fluid and a modification in styling induces a modification of flows in a continuous manner**.

%%%
    ![The default way a webpage react to font-sizes live editing](/images/articles/basealign/grow_default.png){: }
    
    %: The default way a webpage react to font-sizes live editing.

### The magnetism of restriction

If you've ever done print publishing using web-standard you've probably encountered this.
_Constant vertical rythm_ is done by setting certain values to explicit multiples so that a defined rythmes is able to repeat.

%%% figure-full
    ![A two-columns example of a Constant vertical rythm layout](/images/articles/basealign/scheme_constant.png){: }
    
    %: A two-columns example of a _Constant vertical rythm_ layout.

This approach is often desired when you have text flowing on two columns, because it will make side by side things look aligned. However this rythm is also sometimes used on the web (by more nerdy or intricate designer or typographer) to get a magnetized feeling in the rythm of a page.

In _Constant vertical rythm_:

* the boxes of the elements aligns on a common vertical grid. 
* the baselines of the left and right column align with each other but only for element of exact same styling, meaning same _font metrics_ and `font-size` (a paragraph from the left column and one from the right, but not a paragraph from the left and a title from the right who can be more and more misaligned as the font metrics differs).

Changing **the style of the elements is independant of the flow of the rest of the document**.

%%%
    ![How _Constant vertical rythm_ react to font-sizes live editing](/images/articles/basealign/grow_constant.png){: }
    
    %: How _Constant vertical rythm_ react to font-sizes live editing.

This make it easy to adjust your `font-size` live through the inspector, without creating misalignment in the rest of the document, which again can be nice for printing.
Note that if you have big font-sizes, it's possible that some texts vertically overflow their HTML element. 
If such text as to fit in more than one line, it can become handfull to set its `line-height` to be the double of the other (as explained here: [css-baseline-the-good-the-bad-and-the-ugly](https://www.smashingmagazine.com/2012/12/css-baseline-the-good-the-bad-and-the-ugly/#the-bad-improvising-for-variety>))

### The craft of alignement

But as stated, what we want to achieve is a bit more complex than _Constant vertical rythm_...

In a _Base-aligned rythm_:

* the boxes of the elements doesn't aligns anymore on a common vertical grid. 
* but the baselines of all elements aligns on a common vertical grid (even for elements of different _font metrics_ and `font-sizes`).

%%% figure-full
    ![A two-columns example of a Base-aligned rythm layout](/images/articles/basealign/scheme_basealign.png){: }
    
    %: A two-columns example of a _Base-aligned rythm_ layout.

The subtetly is that the part that is aligned with the grid is the top and bottom of the elements's boxes, not their baseline. 
But we could hope for even more: to set up a _base-aligned_ system that, in term of font-sizes changes, works so that **it doesn't un-aligned any element before or after and the element stays base-aligned**.
At this point you could be asking yourself _"But, would it be possible to have an implementation of base-aligned rythm that work in CSS only, without having to manually edit or translate each different styling?"_. 

(the answer is yes)

%%%
    ![How a _Base-aligned rythm_ react to font-sizes live editing](/images/articles/basealign/grow_basealign.png){: }
    
    %: How a _Base-aligned rythm_ react to font-sizes live editing.


## Crafting a constant vertical rythm

In order to be able to do a _Base-aligned rythm_, we first need to implement _Constant vertical rythm_.
While there exists different way to do so, the basic mechanism is the following.
A basic _height unit_ is choosen, you can think of it has the smallest step of your vertical grid. 
It is often equal to the `line-height` of a standard `p`, or a fraction of it if we want to go more granular.

For every elements that compose the typographic flow:

1. If their heights are determined by their text content (like `p`, `h2`, `ul`, ...), they must have a `line-height` which is a multiple of the _height unit_ (often equals).
2. If their heights are not determined by their text content (like `img`, `aside` with fixed height, ...), they must have a fixed `height` which is a multiple of the _height unit_.
3. the sum of their spacing, meaning the `margin` + `border-width` + `padding` that separate each combination of two elements, must also be a multiple of this _height unit_.

We see that if this is respected, then this implies that the height and spaces between every elements that compose the typographic flow are equal to a multiple of the _height unit_, and thus matches our vertical grid.

I love to craft this implementation by introducing a global `--lh` CSS custom properties which define the _height unit_ of our vertical grid. We can then start using this properties as our new unit:

* for the `line-height` of the standard `p`, 
* but also to leave a whole line empty between two `p` by setting their margin to `margin: var(--lh) 0;`. 

The actual CSS unit that we used for `--lh` is also an important choice: `rem`, meaning its going to be relative to the `font-size` defined in the `html` tag, unlike `em` which adapts to the current element `font-size`. 
The idea is that by using `rem` we keep it independant of the elements indivual `font-size`'s, ensuring this value is _global_ over the document, but we stay flexible: by changing the `font-size` of the `html` tag, from `1em` to `16px` to `12pt` for example, it'll automatically adapt our `--lh` to switch unit and size while keeping the same relative ratio.

<!-- For the web, this particularly important to let the `html` tag itself in `em` (`rem` would make no sence here), because [people or devices can changes the default 16px font-size of their browser](https://nicolas-hoizey.com/articles/2016/03/02/people-don-t-change-the-default-16px-font-size-in-their-browser/). In a way, you can see setting the `font-size` of the `html` tag to a fixed unit like `px` or `pt` as: creating a barrier between the user settings and your layout. -->

<!-- For print however, this barrier can be a plus: to ensure everyone is going to print the same thing.
In such situation, it is even preferable to pass this global unit to `pt`.
Note that because everything else (`--lh` and `font-size`) is in `em`, it is relative to this `pt` unit.
For print, you also want to make sure all those value are integer to avoid bad approximations [TODO: add ref]. -->

Another neat craft is to use a class (here `.cvr`), this is so we can target parts of the page to follow a _constant-vertical rythm_ by putting this class on the container; and things like menus, buttons, aside, or other are not going to be messed up and can still have their independant `line-height`, like probably of `1` after a [reset](https://meyerweb.com/eric/tools/css/reset/). This implementation, could be used for both print and web and is relatively easy to implement.

```CSS
:root{
    --lh: 1.35rem;

    /* font-sizes */
    --fs-big: 2.6rem;
    --fs-plus: 1.4rem;
    --fs: 1rem;
}

html{
    /* main font-size, 
    has to be defined in the html tag 
    so we can use the rem unit for --lh */
    font-size: 1.125em;
    /* this value upscale 16px to 18px */
}

.cvr{
    font-size: var(--fs);
    line-height: var(--lh);
}

/* font-sizes */
.cvr h1{
    font-size: var(--fs-big);
    line-height: calc(var(--lh) * 2);
}
.cvr h2{
    font-size: var(--fs-plus);
}

/* spaces */
.cvr :is(h1,h2){
    margin: calc(var(--lh) * 2) 0 calc(var(--lh) * 1);
}
.cvr p{
    margin: calc(var(--lh) * 1) 0;
}
.cvr img{
    display: block;
    height: calc(var(--lh) * 5);
    margin: 0 auto;
}
```

<!-- Unifortably, this breaks when exporting as pdf from firefox. 
However this is possible with svg pattern styled in CSS, but this is whole other story. -->

### Background on a span and dreaming about lh

Looking at the previous implementation we can start to wonder.
**An interestning way to interpret it, is to see it as the implementation of a custom CSS unit**.
Like `em` (typography relative) or `px` (screen relative), we introduced a new typographic relative unit `--lh`, and we count this unit by using `calc()` which of course doesn't look super appealing.

```CSS
/* writting this */
margin: calc(2 * var(--lh)) 0;

/* is conceptual like writting this (which doesn't work) */
margin: 2lh 0;
```

As a reminder the `em` is a conceptual square where its size have the lenght of the font metrics from _ascender_ to _descender_. So let's say that in the font's metric _ascender_ is at 624 and _descender_ is at -400, then the _em square_ is a virtual square of 1024 font units. When we say `fontsize: 16px;` in CSS what we say is: _"make the em square 16 pixels"_, meaning from _ascender_ to _descender_ it's going to take 16px, it's like saying _"make the conceptual invisible global bounding box of the glyphs in my font take 16px"_. This shows that `em` is not really a visual unit, `fontsize: 16px;` doesn't make anything visual on your screen take `16px;`. 
By declaring _"the vertical margins are equal to (or a fraction of) the em-square"_ we end up with margins equal to an invisible conceptual square induced but the crafting of the font itself.
But in fact there is one visible element taking 1em: the selection pseudo-element background that you can see by selecting text, or more generally by putting `background` on a `<span>` (or any inline element).

While using `--lh` as margins, we're declaring _"the vertical margins are equal to exactly (or a fraction of) an empty line of text"_. 
Our eyes percieved it in a different way, it feels like you would have put a `<br/>`, like simply pressing enter on a simple text editor.
Wether one would like to use an invisible line or an invisible bounding box of a glyphs as a spacing unit is a matter of typographic desired feeling that mixes history of lead characters and digital text editor. Anyways, a cool thing about the previous `--lh` unit implementation is that it became available a lot of different places, UI elements heights, or even horizontally.

### Displaying the grid

```CSS
:root{
    --lh: 1.35rem;
    --vg-width: 1px;
    --vg-color: cyan;
}
.show-baseline {
    background:
        linear-gradient(white rgba(0,0,0,0),
        rgba(0,0,0,0) calc(var(--lh) - var(--vg-width)),
        var(--vg-color) calc(var(--lh) - var(--vg-width)),
        var(--vg-color) var(--lh));
    background-size: 100% var(--lh);
}
```

## Crafting a Base-aligned rythm

### Naïve approaches

Starting from a _Constant vertical rythm_, a naïve approach would be to vertically translate everything, let's say with either a `margin-top` or `padding-top` and hope every baselines match the grid, which fails because **every elements with different styling may need to be shifted from a different value**.

%%% figure-full
    ![A two-columns example of Constant vertical rythm where every elements is vertically translated from a same value](/images/articles/basealign/scheme_padded.png){: }

    %: A two-columns example of _Constant vertical rythm_ where every elements is vertically translated from a same value.

So we could precise a different vertical translation value for every elements, as explained in this article [css baseline the good the bad and the ugly](https://www.smashingmagazine.com/2012/12/css-baseline-the-good-the-bad-and-the-ugly/). 
But this is not the solution we are looking for. **Not only because it's long to setup, but also because what we want is get the system right and then think outside of the system we've made**. With this solution, everytime we change the `font-size` of a elements, we would have to adapt this value so it matches with the grid again. Having to adjust the system everytime we move something makes our thought oscillate between what we are doing and how to do it.

Futhermore putting this shift value in `em` for every element as part of their `margin-top` doesn't solve it either. 
As the `line-height` is fixed - not relative to the `font-size` of the elements - and some element have different `font-size`, it can gives us an instinct on way it's a bit more tricky. Let's put it this way, if an element double it's `font-size`, it doesn't imply that the space between their _baseline_ and the bottom of the line-height has doubled, in fact a bigger `font-size` can even make this value change sign.

### Relative shift computations

**Every elements need to be vertically translated, yes:
from the space between their _baseline_ and the bottom of the _line-height-area_.**
We need to look a bit closer at the invisible boxes of web typography.
There is this beautiful article about [css font metrics](https://iamvdo.me/en/blog/css-font-metrics-line-height-and-vertical-align#line-height-problems-and-beyond) that introduces the difference between the _content-area_ and the _line-heiht-area_. This concept allow us to understand why adding an element with smaller `font-size` but with same `line-height` can paradoxically increase the height of the _line-height-area_ (usually like footnotes indices).

%%%
    ![The shift between the bottom of the content-area and bottom of the line-height-area](/images/articles/basealign/shift.png){: }

    %: The shift from _Constant vertical rythm_ and a _Base-aligned rythm_ is the one between the bottom of the _content-area_ and bottom of the _line-height-area_ for every elements.

The _content-area_ from ascender to descender of the font is vertically centered in the _line-height-area_. 
So we have two even spaces at the top and bottom of our line separating the two. 
The property of centering the _content-area_ in the _line-height-area_ is caused by the default `vertical-align: baseline;` declared on our block element containing the text (here a `p`).
What this does specifically, is that it first aligns every _content-area_ between them by their _baseline_, then center the group containing all those _content-areas_ vertically in the middle of the _line-height-area_.

<!-- An important note is that there is no `line-height` value to automatically make the _line-height-area_ equal to the _content-area_.
Unitless values are going to make the `line-height` proportional to it's _em square_, not to the _content-area_.
The value `normal` can sometimes fix this, as it looks for font metrics inside the file, but in lot of cases it will result in gap between the element and its container. -->

We can now express our translation value of each element as the sum of:

1. the space between **the _baseline_ and the bottom of the _content-area_**.
2. the space between **the bottom of the _content-area_ and the bottom of the _line-height-area_**..

The _first shift value_ only depends of the font itself, has shown in the previous figure. This _content-area_ depends on the font-metric of our font, and can not be changed through CSS. Note that it actually is the _descender_ of the font, from the baseline and the bottom, so this value can be expressed in `em`.

The _second shift value_ can be computed as `(line-height - content-area-height) / 2`.
Which depends of both the font-metrics, and the line-height that we set up in CSS. 
But because _content-area-height_ is the sum of ascender and descender of the font, we can express it as `(line-height - (asc / desc)) / 2`.

By giving a name to this special value `rel_bl = (asc - desc)`, the whole _shift_ value could be expressed as
```
  desc + ((line-height - (desc + asc)) / 2) 
= (line-height - (asc - desc)) / 2
= (line-height - rel_bl) / 2
```

The point is that we don't really care about a more specific expression for `rel_bl`, as it only depends of the font-metrics, so we know it can be expressed in `em`.
For the same reason this value is unique per font, whatever `font-size` or `line-height` we give to each elements. 
So finding it manually become quite easy by editing through the inspector. 

<!-- In the case the `line-height` is smaller than the _content-area_, it works exactly the same, onyly the shift value can change sign for certain elements. This also makes it clear that this value could not simply be a unique font-relative `em` value, but need a `calc` that includes the `line-height`. -->

<!-- fractionnal line-height on firefox (note - see other .md on debugging) -->

### Implementation of Base-aligned rythm

This implementation has to follow the one of _Constant vertical rythm_.
It works by shifting every single block contained in our _Base-aligned rythm_ class from a value that is relative to both the _font's metrics_ and the fixed _height unit_ (defined by `--lh`).
For every different font we have to manually find the `--rel-bl` which is the part relative to the _font's metrics_, this can easily be done by setting up the document then increasing and decreasing the value in the inspector until it matches the _vertical grid_, also drawn on the page with CSS.
The elements are vertically translated using `position: relative;` and a `top` value, to ensure their translation doesn't move the initial position in the flow of any of the others.


```CSS
:root{
    --font-1: 'Junicode';
    --rel-bl_font-1: 0.6em;

    --font-2: 'Liberation';
    --rel-bl_font-2: 0.7em;
}

/* GLOBAL */
.ba :is(h1,h2,p){
    /* by default everything as a total lh equal to lh */
    --lh-total: var(--lh);
    line-height: var(--lh-total);

    /* by default everything as a rel-bl of the main font */
    --rel-bl: var(--rel-bl_font-1);
    /* and is in this font */
    font-family: var(--font-1);
}

/* EXCEPTIONS */
.ba :is(h1,h2){
    /* those changed font */
    font-family: var(--font-2);
    /* thus must change their --rel-bl value */
    --rel-bl: var(--rel-bl_font-2);
}

.ba h1{
    /* those double their line-height */
    --lh-total: calc(var(--lh) * 2);
}

/* ALIGNMENT */
.ba :is(h1,h2,p){
    position: relative;
    top: calc(calc(var(--lh-total) - var(--rel-bl))/ 2);
}
```

%%% figure-full
    ![A two-columns example of a Base-aligned rythm layout](/images/articles/basealign/scheme_basealign_2.png){: }

    %: The example explained in the code snipped above, of a _Base-aligned rythm layout_ with two fonts.
    The headers are set in [Liberation Bold](https://github.com/liberationfonts/liberation-fonts), which has different metrics than the Junicode.

Because the `--rel-bl` value is relative to _font's metrics_ it is possible that you have to change it for a bold or italic version of a font, depending of it's drawing.
On a document with two fonts as show in the example, once those values are found, we can style every elements as we want and we don't have to adapt any of those values anymore and everything will stay in place griddier than ever, whatever the typographic styles.


## Rythms and their grains

By using relative positionning on every element &mdash; desynchronising every individual boxes of the native flow of HTML &mdash; it is clear that the above methodology goes quite against [The Web’s Grain](https://frankchimero.com/blog/2015/the-webs-grain/)

> The web is forcing our hands. And this is fine! Many sites will share design solutions, because we’re using the same materials. The consistencies establish best practices; they are proof of design patterns that play off of the needs of a common medium, and not evidence of a visual monoculture.

For printed material made out of HTML & CSS, the akward zigzag of the code is no more present after printing, and what we see is the _baseline_ of the text, while their weird boxes become totally invisible artefact of the craft.
We could say that we finally have here a nice solution to make the DIY practice of CSS-based printed layout benefit from the classic elegance of the academic typographic rules.

However, by becoming it's own culture of alternative to Adobe and proprietary softwares, the web to print practices has inevitably developped it's own grain.
It is clearly revealed to me when I can see the artefact of CSS printed on paper: some words circled by colorfull ellipsis whose dimension where entierly defined by the length of the contained word recognising a `border-radius: 50%;`, or the rawness of spacing andabsence of typographic adjustment, or some quite organic and random `display: flex` or punk-ish `linear-gradient`. 
Those type of process afterfacts can trigger some kind of childlike expression on my face because they delicately unveil the singularities how the tool used.
And because web2print is part of a somewhat ideological mouvement trying to take another path from the hegemonic way of thinking print layout (imposed by the closed workflow of adobe), **all those typographic artefacts becomes the aesthetic of a counter-culture**.

<!-- (if you don't know about it name a few: html2print by osp, paged.js, prepostprint) -->
<!-- In my case, as stated in the [context](#context) with the whole layout being constructed around a `0.5cm` grid, what my eyes wanted to see was not the elements's boxes but an obsessive play on the gridiness of their baseline positionning.  -->

%%% figure-full
    ![](/images/articles/basealign/irma/es_1968_43_touched.png){: }

    %: _Eigenschriften_, Irma Blank, 1968.

For me as a designer, the pleasure of investigating those questions where certainly not about stating what is more elegant, or trying to fill a hole in something: it is about technical curiosity and exploring different patterns and rythms and learning about the way they feel. In the end this text is, once again, about opening the boxes, and by doing so opening the choices.
In hope that you can wonder the right position at the right time between the convenience of simplicity and the precision of constructions.

<!-- ## References

* [The Elements of Typographic Style Applied to the Web](http://webtypography.net/toc/)
    * [Choose a basic leading that suits the typeface, text and measure](http://webtypography.net/2.2.1)
    * [Add and delete vertical space in measured intervals](http://webtypography.net/2.2.2)
* [CSS Baseline: The Good, The Bad And The Ugly](https://www.smashingmagazine.com/2012/12/css-baseline-the-good-the-bad-and-the-ugly/)
* [Deep dive CSS: font metrics, line-height and vertical-align](https://iamvdo.me/en/blog/css-font-metrics-line-height-and-vertical-align)
* [The Web’s Grain](https://frankchimero.com/blog/2015/the-webs-grain/) -->


<!-- * https://gist.github.com/lzielinski03/22ffa07c935380d61ad28d9c99390195
* https://vimeo.com/17079380 -->