title: collective structures for collective
tags: collective, tool
date: 2024 02
status: draft

## La tyranie de l'absence de structure

I think my starting of looking at such collective tools where with a traumatized face after been exposed to the collective dissonance, precariousness and tiredness, unreal process and expectations, lack of concrete future envisionning, invisibilisation of hard to quantify work, knowledge or power gatekeeping resulting in lack of agency, after 1 year of my first real collective work.

This reference is one that to me perfectly shows how collectivity can and do suffer.

## Meets

From personnal experiment in OSP, and probably borowwing from its members insights in other collectif such as Varia.

Do meeting in the same physical space, with an etherpad for support.
Everybody has access to the pad, and can add a point in the next meet agenda whenever they think about it, even if they don't come (this can be a voice of expression).

when adding a point in the agenda it is important to precise the matter of discussion:

* **info**: sharing information to the others
* **process**: establishing a process for the future, like a collective todo list
* **decisions**: taking a decision together

along with an estimation of the time it can take (5min, 15min, 30min).
the clarification of the nature of the discussion can be a bit unusual at first but it allows an effectiveness, and avoid confusions at time of meeting, such as the classic _"I didn't understood that we were going to that decissions right now"_, or everybody freely discuss about budget for example but no process or decisions is taken in the end in any direction and we postpone another free discussions.

Decide of different role in the start:
* **timekeeper**: together with the others, they start by adding a time value to every point in the pad if not done.
* **facilitator**: introduce the points if nobody initialy starts speaking about it, grant speaking turns if multiple persons want to talk at the same time, close a point by looking back at the nature of the discussion, has the information been shared, the process or decision been taken and tasks clearly attributed to persons or sub-groups, if they can't a solutions is to continue the discussion next meet, to respect the timekeeping.
* **notes taker**: take note in the pad, the idea is not that everything is there, but more to have a summary for the absent or to remember.

timekeeping is fundamental is long meetings make meeting not nice and thus collective effort start to dismantle.
it is better to not speak about everything but feel happy and effecient after a meet, that try to rush or elongate and be tired and confused.

discussions should not happen without being timekept and note taken, even if there can be some tentative to have "informal but important" discussions right before or after, or in between two points.

on the pad we take note of who did what role (people usually just propose themselves in informal discussion).
a person can not do the same role in two consecutive meetings, ensuring a certain rotation.

a cool option is to begin the meeting by a round on a fun question that the facilitator decide. 
often questions about the current period of time we are living are cool.
here is some example:

* do you feel ready for winter, how is it rated in your season ranking?
* what work you or others did inspirate you lately?
* do you have nice events in the coming months you're excited about?

## En finir avec les chef·fes

Linked to **meetings** but also to **projects management**.
This was communicated by my friend Luna, after i told her about personnal experience on project: if i stop motivating everyone, administrating everything and all that, then nothing happen, and at the same time certain person where expressing that they didn't felt represented by the direction the project was taking. i was a bit frustrated because i was focus on the idea that if you want something to feel like yours, you have to make proposition to the collectivity, and to take initiative, and that it is normal that the project look like the person that does this job. but also i was aware of the difficulties that many person can encounter to express themselves in a collectivity.

What i love about this is that it show how this is a both way process. 

## Starhawk talisman


## Collaboration agreement

the thing i tried to write about OSP commisionned work

if the collectivity does commisions it is super important to not forget the monetary, precarious and collective work of it.
meaning that in a world were time, skills and energy are limited (not everyone can be there equally, or know, or take the time to learn the same thing, neither they should be paid equally).

## ASBL / VZW

![]()

* From what I've learned it is a practice to have an **external board**. As the Board can deleguate as much as they want, the effective members can become the ones having all responsability (comptes, budgets, decission, etc) and the Board can be distant to the everyday activities of the ASBL. Then trust become important, as the Board detains power but the member have the responsabilities. Think about fire / earth balance in the talisman.