title: free palestine
sort: float
date: 2023 11

![](/images/Flag-Palestine.webp)

* [BDS in belgium](https://www.bacbi.be/cult/Home-bacbi)
* [Open-source designs 'Stop Genocide'](https://www.dropbox.com/sh/pupxap5sisr8i25/AAD40OQ1FNLPXJ9JMmty1n5ya?dl=0) by Atelier Brenda.

<!-- * [Call for Artists and cultural workers](https://docs.google.com/forms/d/e/1FAIpQLScPCxLZE0oSMFKWlQ31AehO26cbrAixlLC-4w0HQ5mIxKISOQ/viewform?fbclid=PAAaYZ7qx5vzlemrVyhQwgzyiKcTKabmsy0i4mHrbRXmVFlL-o6vJV0K7urxc_aem_ASNQzSpMLrAG-3oxaXm1E0WT0wJL1RHJZmUQr3VmG9s6hdFSMw8E3HkUGi6hGDlz-6I). Depending on the structure you are part of, you can work towards a collective declaration of support. -->
<!-- * [OSP Call response](http://blog.osp.kitchen/texts/statement-of-solidarity-with-palestine.html) -->
<!-- * [Palestine Action Toolkit](https://palestinianfeministcollective.org/freedom-within-reach/) from two years ago by Palestinian Feminist Collective. -->
<!-- * [reading list organised by thematics](https://decolonizepalestine.com/reading-list/) by Decolonise Palestine. -->

<!-- * \[petition\] [Condamnation de la violence d'Israël](https://dekamer.mijnopinie.belgium.be/initiatives/i-905) -->
<!-- > As designers we have no audience per say but a network of collaborators and platforms accumulated over years. We believe that those platforms and communities can be political tools. We contacted our collaborators and relayed this [call](https://docs.google.com/forms/d/e/1FAIpQLScPCxLZE0oSMFKWlQ31AehO26cbrAixlLC-4w0HQ5mIxKISOQ/viewform?fbclid=PAAaYZ7qx5vzlemrVyhQwgzyiKcTKabmsy0i4mHrbRXmVFlL-o6vJV0K7urxc_aem_ASNQzSpMLrAG-3oxaXm1E0WT0wJL1RHJZmUQr3VmG9s6hdFSMw8E3HkUGi6hGDlz-6I) with an intention of mutual responsabilisation as an action of support.
>
> We invite the groups for which we build design to use their platforms according to this call. We hope that it can help opening up spaces for: spreading information, dissusions and symbolic or material actions within the Brussels cultural sector.
>
> We invite other design studios to do the same, and think about their position as builders of identities and visibility in time where unambiguous political positionning can impact the public opinion and the position and action of states.
>
> OSP -->


<!-- international -->

<!-- * \[petition\] [Amnesty's call for immediate ceasefire](https://www.amnesty.org/en/petition/demand-a-ceasefire-by-all-parties-to-end-civilian-suffering/) -->

<!-- * [films about Gaza](https://www.palestinefilminstitute.org/en/unprovoked-narratives) by The Palestine Film Institute.

trusted charities

* [Medical Aid for Palestinians](https://www.map.org.uk/donate/donate)
* [Palestine Children's Relief Fundation](https://pcrf1.app.neoncrm.com/forms/gaza-relief)
* [Palestine Red Crescent Society](https://www.palestinercs.org/en)
* [Baitulmaal](https://baitulmaal.org/)
* [Middle East Childrens Alliance](https://www.mecaforpeace.org/) -->
