title: Elements by Element
date: 2020 02
tags: print, declarative, structure, symbol
summary: prints graphic interpretations of the file folder structure of a USB storage device, according to different themes.
thumbnail: /images/projects/element.jpg

## System

<!-- 🝙 ~ 🟄 ✿
&#x1f9f1;&#xFE0E;
&#x1F4A7;&#xFE0E;
&#x2728;&#xFE0E;
&#x1f33c;&#xFE0E; -->
<!-- 🧱💧✨🌼 -->

Elements by Element is a system that prints graphic interpretations of the file folder structure of a USB storage device.
The visual translation process change according to different themes, there is four of them at the moment:

* block
* water
* star
* flower

<!-- FR:
Elements element est un système qui imprime sur papier une interpretation graphique la structure d'arborescence de dossiers-fichiers d'un dispositif de stockage USB.
Interprétée selon différente thématique: block, eau, étoile et fleur. -->

Once installed, it can take snapshots of any usb storage devices, like a visualisation of its inside space at a certain time.
The result is a graphically readable space: we can count the number of files, see the folders inclusion, while proposing a new narrative.

## Installation

The system - a usb hub, a computer and a printer - is displayed on a table, as a small stand that I manage during the exhibition.
People are invited to activate it by coming with their own usb disk and choosing a theme they think fit the best for them and their datas.
They can have their own custom poster printed and sold as a unique artwork.

## Data Narratives

We constantly build sensibilities arround files: an .mp3, an old photo album from 2006, a software;
and to where they are may it be in our pockets or in big centralized servers.
Navigating through data and using file systems is now an ordinary part of our daily live, though it requires abstraction and the construction of mental spaces.
At the same times the global picture of giant file systems in the world right now seems out of touch
and to hold of some sort of mythologies.

This visualisation system explore the use of, constantly needed, narratives to approach abstract data structures.
By using graphical simplified symbolic languages it reflects our data sensibilities, patterns and agencements according to different ontologies.

<!-- l'acte de stocker et naviger a travers des systèmes de fichier est devenu banal.
	or il repose sur des processus d'abstraction et de construction d'espace mental.
Comment est-ce qu'on aime personnelement se référé, appréhender ces espaces de stockages virtuels, ou a nos fichiers
(qu'on developpe une sensibilité vis a vis de certain fichier -peur de le perdre- ou qu'il existe dans ces espaces sans que l'on le sache)
sont gardé et ranger, voyage et revienne.
Le choix de l'user explore cette sensibilité.
Ce snapshot d'un moment précis d'un disque dur permet avec des...
Ces langages symboliques simplifiés permettent de visualiser nos agencements de fichiers selon différente ontologie.-->

## Web Tech

The process use *html* and *css* technology in order to give a graphical form to the arborescence structure.
Because of the very nature of *html*, this allows an almost automatic transcription of:

1. a tree structure encoded as text to
2. a tree structure where the elements contain themselves graphically in a two-dimensional space.
<!-- This is because of the direct nature of html, a text langage that describe relation between -->
<!-- FR:
Le processus de transcription utilise la technologie html et css pour donner une forme graphique à la structure d'imbrication,
ce qui permet une transcription presque automatique d'une arborescence sous forme de texte à une arborescence imbriqué graphiquement
 d'en un espace bidimensionnel.-->

## Installed

This project was presented the 28/02/2020 on the occasion of the [Algomancia](http://copyright.rip/) exhibition, at [Erg Gallery](https://www.instagram.com/erg_galerie/).
