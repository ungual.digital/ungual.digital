title: Actual Art Tips by Actual Artists
date: 2019 03
tags: installation, tutorial, poetry, algolit
summary: an installed system that extracts and broadcasts the most popular art tips from Youtube, one sentence per day.
thumbnail: /images/projects/tips.jpg

## Installation

Actual Art Tips by Actual Artists is an autonomous installed system.
Situated at a chosen location, it extracts and broadcasts *art tips* from Youtube as text on a screen, one sentence per day.

The process is entirely done with code, allowing the installation a certain form of autonomy: everything activates by itself provided that it is plugged and can connect to the Internet.
The process of extraction and broadcast works in real time, meaning it follows the Youtube trends in a live fashion.

## Code

The script takes random sentences, using the subtitles,
from the current most popular *Art Tutorial* videos on Youtube.
It select a sentence from a video and then displays it as text on the screen, shown without precise informations of its origin context and presented as *art tips*.
The script is activated at a fixed frequency, like once a day.
<!-- but it can be changed to a faster frequency (1/hour) during special event like exhibitions to fit the social rithmic. -->

## Translations

By continually broadcasting art tips, it works as a *link* between Youtube contents and a situated physical space.

The material undergoes a translation process, firstly from cultural environement, emphazed by a certain decontextualization of the presentation.
The change of the speed of diffusion, becoming meditative and minimalist instead of the overflooding attention economy driven speed of the internet,
also take part in this translation process.

So it slowly contaminates the new environement, infusing it with a specific content/culture of internet.
But it also allow the material to become something else for the viewer: a poem, a personnal message, a daily ritual.


<!-- ## Tutorial

In this context of the art school, it clearly interogate the idea of learning art particulary in art institutions.
How it affected art creation in the school ?-->

## @ERG

The first instance of the system was installed and activited during one whole year at ERG (École de Recherche Graphique).
The choosed location was on the outside wall of a small pirate library, in the entrance hall of the school.

In this context of the art school, it played with the idea of learning art, particulary in classic art institutions.
<!-- The architecture of the pirate library was designed and constructed by me and Joseph Dofny. -->


<!-- <p>
Actual Art Tips par Actual Artists est un système automatisé qui extrait et diffuse des astuces artistiques des tutoriel les plus populaires de Youtube, une phrase par jour.

Les “tips” extraits sont affichés sous forme de texte sur un écran, sans autres informations et présentées comme des astuces artistiques, une par jour. Le processus est entièrement réalisé avec du code et fonctionne en direct (suivant les évolutions du contenu de Youtube).

L’installation physique est située dans un lieu publique choisi, elle a été activée pendant une année entière à l'ERG (École de Recherche Graphique). L'emplacement choisi était sur le mur extérieur d'une bibliothèque pirate, dans le hall d'entrée de l'école.

Elle fonctionne comme un lien entre la plateforme de Youtube et un espace situé, perméabilisant/contaminant son environnement. Par les traductions entre différent environnements culturels, le changement de la rythmique de diffusion (plus lent et répétitif, méditatif), et la décontextualisation de leur présentation, ces astuces prennent une forme poétique nouvelle.

  </p> -->

## Books

Different books were made as a way to archives an instance of the system: the program executions through the physical installation.
The parameters that generated each books - like the dates and location of the installation - are referenced on the back cover.

In some ways, it recontextualizes the randomly extracted content into a new particular object or collection.
Because those books consist of tips on artistic creation they are art manuals but also books of poetry, a trace of a particular culture on the internet, an archive of the installation.

The book layout is also completely done with code and automatised.
It was done with *html*, *css* and *javascript*, the same languages that structures Internet.


## Multiplication of Instances

There was only one instance of this systems for the moment but I'm interrested to replicate or move it in other physical places, maybe with modifications.
If you dispose of a space in which you would like to see this happen, you can send me an email!
