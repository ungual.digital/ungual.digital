title: Personnal Data Blade
date: 2018 03
tags: 3D, symbol, algorithm
summary: custom daggers generated with code based on personnal data Facebook archives.
thumbnail: /images/projects/blade.jpg

<!-- Using code and 3D printers, a system was designed to create procedurally generated daggers based on the personnal data Facebook archives. -->

## Code & Design

The project was made in Processing.
The personnal data of the user must be downloaded manually on Facebook in the first place, in the form of a big folder with lots of .json files.
Once imported in the script, some particular datas are extracted from the user's archive like the total number of friends, the average time of day of connection, the speed of profile pictures change, etc.
Those values are then used as variables to procedurally and continiusly generates a 3D model of a dagger.
So users with very different uses of Facebook will have very different dagger shapes equivalent.

<!-- Les données de la personne sont téléchargées au préalable sur Facebook sous la forme d'une archive html, cette option est accessible dans les paramètres du compte.
Une foi importée dans le programe, cette archive est traité et certaines valeurs particulières sont analysés (tels que le nombre d'amis, la periode moyenne de connection, l'intensité de changement de photo de profile, etc).
Ces valeurs sont ensuite utilisées comme paramètres pour générer une dague en 3D, sur base d'un design procédural continu.
Le design procédural de la dague est continu, dans le sens ou une légère variation d'un paramètre entraine une légère modification de sa géométrie.
Les formes de dagues sont aussi varié que les profiles Facebook associés sont différents entre eux. -->

The 3D model can be directly exported from the script as an .obj file and then 3D printed.

## Symbol

This allow a re-materialization of our Facebook datas as a physical objects.
Once our dagger is 3D printed, our personnal datas becomes *something we can hold in our hands*, that as a place and utility in the physical world.
This re-materialization process echos the invisible and vaporous nature of digital datas.
<!-- The system therefore aims to allow a re-materialization of our Facebook data as a physical object, something we can hold in our hands. -->

By allowing the users to create a symbolic weapon out of their own extorted datas, it is a political reappropriation.
Affirming that datas from social networks are something that belongs to us, the users.

The dagger object is not only a weapon but also a tool, a personnal jewel or a sacred ritual artefact.
This symbols extends on how we define ourself through online medias, something more intimate that act as a reflection of our daily use of internet platforms, as modern rituals.

<!-- Le programme vise donc de permettre une re-matérialisation de nos données Facebook en tant qu'objet physique:
une arme symbolique, mais aussi un outil ou un bijou.
C'est à la fois une tentative de réapropriation politique de nos données sur les réseaux sociaux.
Une exploration de la dimension de définition de soi, comme un reflet intime de nos habitudes sur les platformes en ligne. -->

<!-- Cette dimension se veut évidement critique sur la récupération de nos données personnelles par Facebook
(en jouant d'une métaphore sur le côté tranchant/pointu de la dague: un objet dangereux à manipuler),
mais permet aussi de repenser de façon positive notre rapport intime au réseaux sociaux en envisageant notre compte comme un objet multiple:
un attribut décoratif, une plateforme de rituel, une façon de nous délimiter, un outil pratique malgré ses dangers d'utilisation, etc.
Le prolongement de nos rapports sociaux à travers des plateformes virtuelles tels que Facebook est un type d'augmentation de l'être humain par la technologie à part entière,
et l'impact qu'il opère sur nos perceptions au quotidien se trouve trop souvent négligé.
Ce rapport demande donc impérativement à être re-penser, car il concerne autant le future de l'individu dans sa relation personnel à la technologie,
que l'avancée technologique globale (big datas, reconnaissance faciale, réseaux de neurones, etc.)
et le monde économique, social et politique qui vient avec. -->

<!-- L'image de la dague joue un rôle multiple.
C'est premierement un symbole d'arme tranchante, qui renvoi au côté dangereux et puissant des collections de datas qu'accumulent à notre égard certaines coorporations comme Facebook:
comme pour l'arme la situation dépend de qui l'utilise et comment.
Dans ce sens, le programe nous permet de se ré-approprier nos données personelles et d'en faire une arme, non plus pour les coorporations mais cette foi-ci au service de l'individu qui les as générées. La dague renvoie également au bijoux, à l'emblème. -->

<!-- Ce côté de l'objet explore la dimension du compte virtuel comme accessoire, comme façon de se définir ou d'appartenir.
Par exemple, deux utilisations de Facebook assez différentes meneront à deux dagues également différentes, comme un miroir de nos habitudes.
La dague est souvent employée comme objet de rituel, du sacré.
C'est donc aussi une façon d'attirer le regard sur le côté sacré que peuvent prendre nos habitudes liés aux technologies quotidiennement,
qui finissent par s'encrer en nous comme des rituels modernes.
Une foi imprimé en 3D, nos données se retrouve matérialiser comme "quelque chose qu'on peut tenir", qui à une place, une utilité, une importance dans le monde physique.
La magie de cette matérialisation fait justement echo au caractère invisible et nuageux de ces datas dont on parle -->

## Installation

Six daggers were created based on the Facebook profiles of some of my friends and myself, and then 3D printed.
The daggers where printed at the [iMAL](https://www.imal.org/en) fablab.

The installation consists of those six daggers and 3 infographics posters depicting how the facebook parameters are linked to the geometrical features of the daggers.
This display push to a direct comparison of the shown daggers, leading to playful abstraction games between of shapes and concepts.


## Accessibility

I definitively want to make this script accessible and usable to the public, as well as open-source,
but this was coded in a very DIY punk way... and there are still some bugs to fix.
(the code don't run on lots of platforms, and the 3D model often need manual cleaning/corrections in order to be 3D printed)

As it is a project from the past, I have to find the time and motivation to rewrite it in a more shareable way and to construct a confortable interface.
As soon as I do, I'll put everything on some repositories.
