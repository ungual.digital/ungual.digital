title: Meyboom
date: 2021 11
tags: map, collectivity
summary: Economical webpage for the collectivity of artists at Meyboom. The navigation is a simple handcrafted interactive SVG map.
link: http://meyboom.space/
thumbnail: /images/websites/meyboom.png