title: tekhne.website
date: 2023 10
tags: svg, filter, craft, inkscape, nested
summary: A collaborative website for research on music. The logo is created through SVG filters, taken from Inkscape and translated to the web, changing everytime as some visual generative synth.
link: https://tekhne.website
thumbnail: /images/websites/tekhne.png