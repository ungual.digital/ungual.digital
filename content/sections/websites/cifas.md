title: cifas.be
date: 2023 8
tags: liquid, solid, nested
summary: _"Performing art in the city and its fringe"_. The website is devided into liquid and solid states that nourrishes each others. Tags and people act like desire paths, motivating exploration.
link: https://cifas.be
thumbnail: /images/websites/cifas.png