title: Oscillation festival (2022)
date: 2022 03
tags: sound, event, drawing, collective
summary: 2022 edition of Q-O2 Oscillation festival. The page reshape itself by activating or hidding sections. A dynamic SVG drawing grows according the number of people connected on the page, playing with the idea of audience.
link: http://www.oscillation-festival.be/2022/
thumbnail: /images/websites/oscillation.png