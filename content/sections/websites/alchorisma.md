title: alchorisma.constantvzw.org
date: 2021 04
tags: paths, algolit, walk
summary: Online publication on Constant's Alchorisma worksession. A website where hyperlinks between pages are inserted algorithmically, common words between two texts becomes pathways.
link: https://alchorisma.constantvzw.org/
thumbnail: /images/websites/alchorisma.png