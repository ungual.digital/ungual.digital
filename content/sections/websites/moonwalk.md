title: moonwalks.be
date: 2022 05
tags: walk, change, moon, typography
summary: Moon walks in the Forest organised by Anaïs Berck. The typography vary according to the current phase of the moon. The color theme varies according to wether it's day or night in Bruxelles.
link: https://www.moonwalks.be
thumbnail: /images/websites/moonwalks.png