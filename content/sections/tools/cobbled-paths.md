title: cobbled paths
date: 2024 03
tags: ascii, typography, figlet, plotter, svg
summary: a figlet 2 svgbob.rs 2 hpgl pipeline. it makes a way from the blocky discontinuity of Ascii drawings, to the smoothness of bezier curves, to an anologic pen-plotter interpretation.
link: https://gitlab.constantvzw.org/osp/tools.cobbled-paths
thumbnail: /images/tools/cobbled.png