title: soft studies
date: 2020 02
tags: typography
summary: Generating new typefaces by hydriting together two different pre-existing typefaces
status: hidden

# Hybrid Studies

Pour le moment: documentation et historique de recherche de manière brouillion, en Français.
Une réelle présentation du projet suivrat, avec un repo git et une documentation claire sur les bugs et *to do*.

## Idées de départ

Construire un outil / une interface qui permet de d'hybrider entre-elles des fonts existantes.
A l'aide de quelques sliders et/ou boutons, l'user pourrait remixer des fonts dont iel dispose déjà (input), et enregistrer des nouvelles (output).
L'interface s'apparentrais à un logiciel de DJing, Avec pour base de son fonctionnement un slider qui opère un transition continue d'une font A à une font B. On peut ainsi produire une font hybride qui est un mélange de 50% la font A, 50% la font B (et probablement d'y ajouter d'autre type de transformations ou de paramètres lors de ce processus).
Une visualisation en temps réel de samples text des deux fonts ainsi que du résultat hybride permet d'avoir un feedback direct des modifications, de manipulation presque physique de l'operation.


## Cheminement technique

### Interpolation

La plus grosse partie du projet est l'algorithme d'interpolation de glyph, qui va permettre de créer les caractères hybrides entre deux fonts.
L'interpolation est le fait de deviner des formes intermédiaires successives afin de créer une transition continue entre deux formes données.

Il n'existe donc pas une bonne et unique manière de deviner ces formes, la transition la plus naturelle ou jolie dépend du type de forme, de leur format, du champs d'application, etc. Le choix de l'algo est donc ce qui determine en partie l'esthétique du projet.

### Recherche d'une lib / un algo d'interpolation de forme vectorielles

Après quelques recherches, il s'avère que les outils de ce type les plus utilisés et sont des librairie javascript utilisées sur le web pour faire du tweening. Les web designer / app designer les utilises pour animé des petits icones svg en un autre quand on clique dessus, rendre un menu plys dynamique gràce a des animations organique (bouncing, 3d, etc). Le désavantage est que la plupart de ses librairies sont pensée pour créer une animation fluide et courte, et non que la forme interpoler à mi-chemin soit élégante ou naturelle.

L'algo classique de l'interpolation/tweening svg est le suivant:
1. subdiviser les 2 formes pour avoir le même nb de points
2. faire correspondre les points entre eux
3. tracer l'interpolation

Problèmes récurrents:
* la correspondance entre les deux sets de points est pas naturelle. (morphindex)
* Quand il y a plusieurs path comment savoir lequel doit correspondre avec lequel.

librairie de svg morphing (tweening) en .js :

* GREENSOCK.js:
  * fonctionne bien mais pas open-source, et payante (chère)  .

* SNAP.js:
  * résultat sympa mais glitch sur des formes complexes
  * pas de manipulation sur le morphindex possible si ce n'est pas celui qu'on veut
  * échange des paths entre eux
  * créer une animation entre les formes mais pas d'acces direct aux formes intermédiaires

* KUTE.js:
  * chouette résultat si le morphindex est placé manuellement
  * ne prend pas en compte les multiple paths (partie à écrire)
  * créer une animation entre les formes mais pas d'acces direct aux formes intermédiaires

A force de recherche de plus en plus spécifique je trouve:

* FLUBBER.js:
  * best-guest de comment subdiviser les svg
  * best-guest du morphindex qui fonctionne super bien
  * accès direct aux formes interpolées
  * ne prend pas en compte les multiple paths (partie à écrire)
  * open-source

* this thing constructed over d3? https://pratyushcrd.github.io/d3-path-morphing/
  * also only polygon conversion...
  * seems like flubber for d3 adaptation

[Flubber](https://github.com/veltman/flubber) semble un choix optimal, notamment car il n'a pas été pensé pour du tweening de web-design,
mais pour les designer qui font du data-driven document (schéma complexe, canvas, interface),
donc un domaine qui s'interesse beaucoup plus à a l'aspect 'naturelle' des formes interpollées que juste l'animation entre les deux.
[Super cool vidéo](https://www.youtube.com/watch?v=PLc1y-gim_0) qui présente l'idée d'interpolation vectorielle et les recherches du créateur de Flubber.

Note: j'ai remarqué bien après que Flubber fonctionne en fait en convertissant/approximant la forme en polygon (pas de de courbe) avec énormément de points.
Forcément cela produit des caractères au final sans 'réelle' courbe et avec bcp trop de points.

Note sur:

* abode illustrator

SVG minimal lib (to construct interpolation myself or polyfill)

* Raphael:
  * svgpathparser (the one used in flubber)
  * Raphael.pathToRelative(pathString) et Raphael.\_pathToAbsolute(pathString)
  * NOTE: snap.svg est la même chose que rapheal, mais snap a plus moderne ! :) (autant utiliser snap)

* svgjs:
  * heavily simplify code for svg creation
  * acces path data but not naming control and anchor point (list must be done manually by detecting type)
  * no convertisser

* using old polyfill with path.pathSegList:
  * replace a deprecated function
  * parser with nice object presentation

* using new polyfill with path.getPathData():
  * proto for a not yet implemented method
  * not naming control and anchor point


### Décrypter les fichiers de font

Un autre objectif est de permettre d'importer des fichier de fonts, des les manipuler, et puis de récréer un fichier font à la sortie.
Sur base du conseil de Antoine Gelgon, j'utilise [opentype.js](https://opentype.js.org/opentype.js) qui permet d'ouvrir un fichier .ttf ou .otf et de le parser en un objet javascript qui contient les paths de chaque glyphes, ainsi que les metadata, les dimensions, et autres données de la font.

## Remarques après les premiers test

Une fois l'ouverture de font implémenté avec opentype.js, et l'interpolation basique de flubber, j'ai pu généré les résultats suivant.

* Seul les charactère formé d'un seul path sont faisable pour le moment.
Il faudra implémenté manuellement un algorithme qui essaie de deviner quel path va avec lequel (un scan haut-bas?)
* Flubber à un paramètres de qualité de subdivisions des formes, s'il est élevé il produit des meilleurs résultat mais ça prend plus de temps (1 sec par lettre), ce qui peut vite exploser.
Il faudra peut-être lancer un calcul asynchrone en background pour fluidifié l'utilisation de l'interface.
* L'algo de Flubber agis souvent de manière non-naturelle: comme dans le T large et le T mince ou la forme a 50% à ces angles tronqués plutot que de juste scale la largeur de la forme. La raison est la division du chemin en polygone avec bcp de points.

## Fill de flubber

Flubber demande que les deux paths soient connexes.
Ils les converti ensuite en polygone et produit l'interpolation.

Dans le cas de glyphes non connexes (B - 3 partie, i - 2 parties, etc), j'ai produit un petit fill.
* decompose les paths en composantes connexes
* trie ces composantes en fonction de leur point le plus haut
* regarde si elles ont le même nombre de composante
* si oui alors utiliser flubber en pairant les composantes 2 à 2
Cela fonctionne dans la plupart des cas vu la nature graphique des lettres latines.
Par exemple un i aura probablement deux composantes dans chacune des fonts, et le points sera toujours plus haut que le fut.
Cela ne fonctionnera probablement pas avec des font non-latine, ou trop fantaisistes (on peut imaginer le point du i être plus bas qu'une excroissance de sont fut).

**problème restant:**

* si un i a un point d'un côté et pas de l'autre, le point matchera avec le fut.

* Un deuxième problème est si le nombre de composante ne corresponde pas. Par exemple un g qui en a 3 ou un g qui en a 2. Ou le glyph o qui est formé d'un cercle disjoint d'un côté et pas de l'autre. La solution choisie est que toutes les shapes en excès d'un côté ou de l'autre shrink en leur centre jusqu'à disparaitre.

* Un troisième problème est que flubber se permet de changer l'orientation des composantes, les rendants toutes  du type 'fill'.
Par exemple Les espaces négatifs du B deviennent remplis.
https://pomax.github.io/svg-path-reverse/
  * ou on utilise le even/odd algorithme
  * ou on detect l'orientation de chaque composante (stocké dans l'object d'interpolation)
    si les paths matché on la même orientation on les interpole
    on les re-reverse par après, ceux qui sont nécéssaire
  * on corrige après avec un algo authomatique de detection d'even/odd (fontforge a ça mais c'est en python)

https://stackoverflow.com/questions/35560989/javascript-how-to-determine-a-svg-path-draw-direction
Par soucis de temps j'ai pris la première mais la deuxième serait mieux car c'est la bonne grammaire de chemin à avoir.

Si ces 3 étapes sont 'résolues' on a un outil "fonctionnel".

## Display Note

Une option de changement de taille (zoom) pour mieux appréhender la font hybrid a l’image des thypothèque
Il semblait important (d’aller plus loin que ce que propose les typotheuqe standard) en plus de permettre au zoom, avec une option de panning nécessaire pour se déplacer dans le caractère, afin de réellement apprehender la matière svg de ce qui est produit en terme de point et de courbe - comprendre comment l'algo agis.
Ces fonctions permettre à l’interface de devenir un peu un research lab dans la matière de l’hybrid glyph, plus qu’un display.
Une librairie plus adaptée pour, parser manipuler et display du svg est sans doute mieux que le code que j’ai fait.

Cela mériterait d'ajouter une visualisation des points d’ancrage!

Note: rien ne modifie le svg lui meme, on ne fait que de la transformation globale.

Bug:
* slider en log et un champ pour le remplir manuellement
* if s>1 et pan -> ça but
* center-right-left?
* nightmode redo!

<!-- ## Direction du projet

Se center sur les fonts mainstream dont l'utilisation est devenu un standard.
Par exemple les fonts par defaut des navigateur web (times, arial, consolas, comic sans ms).

Les hybrider ou entre-elles: between two standards, adaptive cliché.  

ou avec des choses qui n'ont rien a voir: perversion des standards, rafraichissement du vieux établis et solide par le nouveau fragile et dynamique.

s'interesser aux conditions légales des fonts samplées, et a comment prend forme les méta-données des nouvelles fonts générées?

Possibilité de changer d’algorithme d’interpolation. Expérimenter différente matière.

Export de font .ttf

Possibilité de changer la quality de flubber (change vraiment le résultat!)

dictionnaire qui retient les interpolator

Algorithme de simplification -->

<!-- ## To DO

1. fonction d'association des path de glyph (pas tjrs bonne odre)
2. cassage des caractère pour mm nombre de path
3. automatisation des formes-contreforme
4. Possibilité d'exporter la font hybride ou juste le texte en input (en .svg)
5. Reglage supplémentaire (qualité, déformation, lissage, taille, etc...)
6. ligatures?

done:
* glyph composés de plusieur path ok
* plusieur glyph en meme temps
* export en svg
* ligne de mesure -->

## References :
* [Helblauw - Thomas Meinecke](https://www.instagram.com/p/B5A4sI6g7Nv/) (Dear reader)
* [Metapolator](http://metapolator.com/pre.html)
* [beowulf](https://www.moma.org/collection/works/139326)
* Opn Korn logo (cultural sampling)
* [lyno font](https://radimpesko.com/fonts/lyno) (impression de between 2 states)
