title: against
date: 2023 11
tags: css, declarative, standard, color, accessibility
summary: A conflicting tool about web named color and accessibility standards.
link: https://against.ungual.digital
thumbnail: /images/tools/against-logo.png