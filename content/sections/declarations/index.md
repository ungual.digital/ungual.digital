title: declarations
sort: float
date: 2023 11
show_in_list: true
summary: [Declarations](https://declarations.ungual.digital) is an ongoing artistic research into the poetic materiality of the CSS web-standard. Declarations is a love letter to the crafts of designing with language.
thumbnail: /images/block/declarations.png
tags: declarative, css

<h1 class="declarations">
    <span>D</span><span>e</span><span>c</span><span>l</span><span>a</span><span>r</span><span>a</span><span>t</span><span>i</span><span>o</span><span>n</span><span>s</span>
</h1>

[Declarations](https://declarations.ungual.digital) is an ongoing artistic research into the poetic materiality of the CSS web-standard. Declarations is a love letter to the crafts of designing with language.</p>

<style>
/* define the flowing and size of the logo */
h1.declarations{
    margin: 0;
    line-height: 1;
    font-weight: normal;

    position: relative;
    z-index: 0;
}

/* flowing and sizing of the typo */
:root{
    --a-color-that-cascades: aqua;
    --logo-x-h: 0.66em;
    --logo-gap: 0.25rem;
    --logo-size: 5rem;
}
h1.declarations{
  display: flex;
  align-items: end;
  flex-wrap: wrap;
  gap: var(--logo-gap);
  line-height: 1;
  font-size: var(--logo-size);
  font-family: serif;
}

/* transform the letters into solid blocks */
h1.declarations span{
    background: white;
    box-shadow: var(--a-color-that-cascades) -0.6rem -0.15em 0.15em inset;
    color: transparent;
    position: relative;
}

/* shape each letter span using border-radius */
h1.declarations span:nth-of-type(1){
    border-radius: 0 50% 50% 0;
}
h1.declarations span:nth-of-type(2){
    height: var(--logo-x-h);
    border-radius: 50%;
}
h1.declarations span:nth-of-type(3){
    height: var(--logo-x-h);
    border-radius: 50% 0 0 50%;
}
h1.declarations span:nth-of-type(5),
h1.declarations span:nth-of-type(7){
    height: var(--logo-x-h);
    border-radius: 0 75% 0 0.2em;
}
h1.declarations span:nth-of-type(6){
    height: var(--logo-x-h);
    border-radius: 0.35em 0 0 0;
}
h1.declarations span:nth-of-type(8){
    border-radius: 0 0 0 0.2em;
}
h1.declarations span:nth-of-type(9){
    border-radius: 0;
    height: var(--logo-x-h);
}
h1.declarations span:nth-of-type(10){
    height: var(--logo-x-h);
    border-radius: 10em;
}
h1.declarations span:nth-of-type(11){
    height: var(--logo-x-h);
    border-radius: 0 0.35em 0 0;
}
h1.declarations span:nth-of-type(12){
    height: var(--logo-x-h);
    border-radius: 10em 0 10em 0;
}
h1.declarations span:nth-of-type(10){
    flex-grow: 1;   
}
</style>