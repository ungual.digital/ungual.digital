title: a web sticker
date: 2023 09
show_in_list: true
summary: a web-sticker rotating at the speed to which practices and tools shapes each others
thumbnail: /images/block/practices.png
tags: practices, tools

<iframe src="https://practices.tools"></iframe>