title: my favorite color
slug: 'my-favorite-color-section'
date: 2023 04

<div id="my-favorite-color-display">
<dl class="pad" style="position: absolute; bottom: 0;">
    <dt style="margin-bottom: calc(var(--lh) * 0.5);">
      <code>hsl(162, 70%, 54%)</code>
    </dt>
    <dd>
      the both intense and conforting sound of something plunging into water and the following sub-bass silence, a deep breath that extends to all parts of the body
    </dd>
<dl>
</div>

<script>
const url = "https://myfavoritecolor.dorian.systems/api.php";
const xmlHttp = new XMLHttpRequest();
xmlHttp.onreadystatechange = function() {
  if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
    document.documentElement.style.setProperty('--clr-fav', xmlHttp.response);
  }
}
xmlHttp.open("GET", url, true);
xmlHttp.send(null);

document.documentElement.style.setProperty('--clr-fav', "hsl(162, 70%, 54%)");
</script>
