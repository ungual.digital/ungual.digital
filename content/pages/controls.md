title: controls
section: false

This page works with horizontal scroll.
There are different ways to use this on web right now:

- `shift` + `scroll` with the mousewheel
- `←` `→` keys
- `swipe` on trackpad or touchscreen
- `drag` the scrollbar
- `click` & `drag` on the floor (added custom control i made)
