my website

using pelican and custom theme

## Wishlist

### dev

* RSS
* scroll-padding for modile toc
* image-process plugin
* opengraph and twitter card for individual pages
* print article possibility

### content

* add squirrel collab to single line text
* post nest text/tuto and feedbacks
* svg diy crafting text
* fav color redo + API
* music lists