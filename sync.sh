#!/usr/bin/env bash

# get variable from .env file
if [ -f .env ]
then
  export $(cat .env | xargs)
fi


# https://lftp.yar.ru/lftp-man.html

# -R,      --reverse                  reverse mirror (put files)
# -n,      --only-newer               download only newer files (-c won't work)
# -e,      --delete                   delete files not present at the source
# -v,      --verbose[=level]          verbose operation

# --parallel[=N]             download N files in parallel
# note: the number of maximum parallels connections accepted by the ftp can vary, if you get an error reduce it.

lftp -e "set ssl:verify-certificate false; open $FTP_HOST; user $FTP_USERNAME $FTP_PASSWORD; mirror -Rnev --ignore-time --parallel=4 ./output  $FTP_FOLDER ; bye"