
from pelican import signals
import re
from bs4 import BeautifulSoup

import logging
log = logging.getLogger(__name__)

config = {
    'EXTERNAL_CLASS': 'external',
    'INTERNAL_CLASS': 'internal',
}

LOG_PREFIX = "[links_genres]"
DOMAIN = "ungual"
TLD = "digital"
MAIN_SUB = "www"

subdomain_regex = re.compile("^https?:\/\/([^.]+)." + DOMAIN + "\." + TLD)
domain_regex = re.compile("^https?:\/\/" + DOMAIN + "\." + TLD)

# TODO
# * config with name for external, internal and local
# * toggle for each in new tab or not
# * note: a link that begin with https could actually be internal, 
#   check if same domain
# * note: a link that don't begin with # could actually be local
#   check if same path following a #

def is_local(a):
    # local (same page anchor)
    n = re.match("^\#", a.get("href"))
    if n:
        return True;
    else: 
        return False;

def add_class(a):
    try:

        # test if it is relative or not
        absolute = re.search(r"^https?:\/\/", a.get("href"))
        classes_to_add = []

        # absolute
        if absolute:

            # test if it is a subdomain
            subdomain = re.search(subdomain_regex, a.get("href"))
            domain = re.search(domain_regex, a.get("href"))

            # absolute internal or sub
            if subdomain:
                if subdomain.groups(1)[0] == MAIN_SUB:
                    # http://www.example.com/something
                    classes_to_add.append('internal')

                else:
                    # http://mysubdomain.example.com/something
                    classes_to_add.append('sub')
                    a["target"] = '_blank'

            # absolute internal
            elif domain:
                # http://example.com/something
                classes_to_add.append('internal')
            
            # absolute external
            else:
                # http://anotherwebsite.com/something
                classes_to_add.append('external')
                a["target"] = '_blank'

        # relative
        else:

            # /something
            classes_to_add.append('internal')
            if is_local(a):
                # /#anchor on same page
                classes_to_add.append('local')


        # adding all new classes
        a["class"] = a.get('class', []) + classes_to_add
        # log.debug('- {h} is {c}'.format(h = a['href'], c = " ".join(classes_to_add)))

    except AttributeError:
        pass

def links_genres(path, context):

    log.debug('{pre} for {path}'.format(pre = LOG_PREFIX, path = path))

    # open file
    with open(path, "r+", encoding="utf-8") as f:
        html = BeautifulSoup(f, 'html.parser')

        # iterate on anchors
        anchors = html.find_all('a')
        for anchor in anchors:

            # add classes
            add_class(anchor)

        # write new file
        f.seek(0)
        f.truncate()
        f.write(str(html))

def register():
    signals.content_written.connect(links_genres)