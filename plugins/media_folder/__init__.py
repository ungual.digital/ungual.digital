from pelican import signals
import os
from distutils.dir_util import copy_tree

import logging
log = logging.getLogger(__name__)

LOG_PREFIX = "[media_folder]"

# TODO:
# * media folder is in the same relative direction from article to inside the images/ folder
#   this allow to not having to copy the img inside, and only add the url, because STATIC_PATH already does

def sourcePath2mediaPath(source_path):
    filename, file_extension = os.path.splitext(source_path)
    return filename + ".media"

def add_media_urls(article_generator):

    for article in article_generator.articles:

        media_folder_path = os.path.join("content", sourcePath2mediaPath(article.relative_source_path))

        if os.path.isdir(media_folder_path):

            print('{pre} found: {f}'.format(pre = LOG_PREFIX, f = media_folder_path))

            # manually copy the folder to the output directory
            save_media_folder_path = os.path.join("output", sourcePath2mediaPath(article.save_as))
            copy_tree(media_folder_path, save_media_folder_path)

            # construct the urls list
            article.media_urls = []
            for media in os.listdir(media_folder_path):
                if os.path.isfile(os.path.join(media_folder_path, media)):
                    # (basename, ext) = os.path.splitext(media)

                    log.debug('- {f}'.format(f = media))
                    article.media_urls.append(os.path.join(sourcePath2mediaPath(article.save_as), media))


def register():
    signals.article_generator_pretaxonomy.connect(add_media_urls)
