#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Doriane'
SITENAME = 'ungual'
SITEURL = 'https://ungual.digital'
CONTACT = 'doriane@ungual.digital'
SHORT = 'digital: relating to a finger or fingers. ungual: relating to a nail, hoof, or claw.'

PATH = 'content'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


# CUSTOM PAREMETERS
# ==============================================================================

# theme path
THEME = 'theme'

# by default pelican asks for a date for every article (?)
# so no date field needed
DEFAULT_LANG = 'en'
TIMEZONE = 'Europe/Paris'
DEFAULT_DATE = 'fs'
DEFAULT_DATE_FORMAT = '%b %Y'
#only change how article.locale_date is displayed
#the metadate is still YYYY MM DD
LOCALE = ('en_US')
#change the lang in which days and month are displayed


# ARCHITECTURE
# ==============================================================================

# category is folder is a section in the home
USE_FOLDER_AS_CATEGORY = True

ARTICLE_PATHS = ['sections']
STATIC_PATH = ['images']

ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'

CATEGORY_URL = '{slug}/'
# no individual page per category
CATEGORY_SAVE_AS = ''

# disable the generation of those default html files
PAGE_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
TAG_SAVE_AS = ''

# the "collections" type html pages that we want
# DIRECT_TEMPLATES = ['index', 'authors', 'categories', 'tags', 'archives']
DIRECT_TEMPLATES = ['index', 'list']


# MD EXTENSION
# ==============================================================================

from markdown_figcap import FigCapExtension
from yafg import YafgExtension

# https://github.com/funk1d/markdown-figcap


MARKDOWN = {
    'extensions': [
        YafgExtension(stripTitle=False),
        FigCapExtension(),
    ],
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'codelight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

# for officially and third party extensions:
# https://python-markdown.github.io/extensions/

# FILTERS
# ==============================================================================

from markdown import Markdown, markdown

import datetime
import pytz

utc=pytz.UTC

def cat_sort(categories):
    # sort categories by their .order meta-data defined in the index file
    # (added to be compatible with the CATEGORY META plugin)

    map = {
        'first': datetime.datetime.now() + datetime.timedelta(days=1),
        'float': datetime.datetime.now(),
        'sink': datetime.datetime.fromtimestamp(0),
    }
    for cat in categories:
        cat[0].sort_date = utc.localize(cat[0].date)
        if hasattr(cat[0],'sort'):
            cat[0].sort_date = utc.localize(map[cat[0].sort])
        elif len(cat[1]) > 0:
            cat[0].sort_date = cat[1][0].date

    cat_sorted = sorted(categories, key= lambda x: x[0].sort_date, reverse=True)

    for cat in cat_sorted:
        print(cat[0].name, "---", cat[0].sort_date)
    
    return cat_sorted

def md_parse(value):
    # to parse yaml field as markdown
    return markdown(value)

def naive_date(e):
    e.date = e.date.replace(tzinfo=utc)
    return e

def sort_by_date(list):
    list = [naive_date(e) for e in list]
    return sorted(list, key=lambda e: e.date, reverse=True)

JINJA_FILTERS = {
    'cat_sort': cat_sort,
    'md': md_parse,
    'sort_by_date': sort_by_date,
}

# PLUGINS
# ==============================================================================

PLUGIN_PATHS = ['plugins']

PLUGINS = ['media_folder', 'category_meta', 'extract_toc', 'links_genres']

# MEDIA FOLDER (custom)
# create an article.media_urls list of urls for img
# based on what is in the folder: media.{{article.filename}}

# LINK GENRES (custom)
# add a class either it's an external, internal or local link
# plus add target=_blank for the external links

# CATEGORY META
# every category folder as an index with meta-data
# here category are exactly the sections on the /home
# (https://github.com/getpelican/pelican-plugins/tree/master/category_meta)
# custom edit line 84-88: add order metadata support

# EXTRACT TOC
# make a article.toc available in template (nice for long articles)
# (https://github.com/getpelican/pelican-plugins/tree/master/extract_toc)
# note that the other pelican-toc plugin, isn't working/maintained anymore:
# (https://github.com/ingwinlu/pelican-toc/tree/b98d89b2cfa857c59b647ef0983a470408d6d8cd)
# for markdown extension options see: (https://python-markdown.github.io/extensions/toc/)

# CODEHILITE
# (https://python-markdown.github.io/extensions/code_hilite/)
# already activated as a markdown extension
# generate css with:
# pygmentize -S onedark -f html -a .codelight > theme/static/css/codelight.css

# add image_process
# add typogriphy