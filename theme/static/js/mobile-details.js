
let details = document.querySelectorAll('.mobile-details');

// start on desktop size
let screen_size = 1;

function close_details(){
    // if mobile close all details
    if(screen_size == 0){
        console.log('closing details');
        details.forEach((d) => {
            d.removeAttribute("open");
        });
    } 
    // else open them all
    else{
        details.forEach((d) => {
            d.setAttribute("open","");
        });
    }
}

function size_change(){
    let new_screen_size = window.innerWidth < (60 * 16) ? 0 : 1;
    
    // screen size changes!
    if (screen_size != new_screen_size){
        screen_size = new_screen_size;
        close_details();
    }
}

window.addEventListener("resize", size_change);
size_change();